#ifndef __USEFUL_H__
#define __USEFUL_H__

#include <stdbool.h>

char *USEFULL_StrExtract(char *begin, int character, int *len);
void *USEFULL_StrFree(char *buf);
char *alloc_n_cpy(const char *string, int len);
char *lowercase(char *str);
char *CopyTillChar(char *string, int c, size_t maxlen);
char *CopyTillChar2(char **p, int c, size_t maxlen);
int char_count(const char *s, char c); // count the number of C occurence in S.
char *char_replace(const char *s, char from, char to);

bool RegexInit(void);
void RegexFree(void);
bool RegexMatch_HTTP_Url(const char *http_url);

#endif