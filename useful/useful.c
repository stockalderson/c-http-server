#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <regex.h>

#include "useful.h"

// Extract string store it into BUF and store BUF length into LEN if not null
// On success return char *buf on error return NULL
char *USEFULL_StrExtract(char *begin, int character, int *len)
{
    if (begin == NULL)
    {
        printf("begin is NULL.\n");
        return NULL;
    }

    /* Declaration */

    char *end, *buf;
    int length;

    /* Initialisation */

    end = NULL;
    buf = NULL;
    length = 0;

    end = strchr(begin, character);
    length = (end - begin)+1;
    if (length <= 1 || length > 1000)
        return NULL;
    buf = malloc(length * sizeof(*buf));
    memset(buf, 0, length);
    strncpy(buf, begin, length);
    buf[length-1] = 0;

    if (len != NULL)
    {
        *len = length;
    }

    return buf;
}

void *USEFULL_StrFree(char *buf)
{
    if (buf == NULL)
        return NULL;

    free(buf);

    return NULL;
}

char *alloc_n_cpy(const char *string, int len)
{
    char *buf;
    buf = calloc(len+1, sizeof(char));
    strncpy(buf, string, len);
    buf[len] = 0;

    return buf;
}

char *lowercase(char *str)
{
    if (str == NULL)
    {
        printf("Str is null.\n");
        return NULL;
    }

    char *string = str;
    int len = strlen(string);
    int i = 0;

    for (i=0 ; i<len ; i++)
    {
        string[i] = tolower(string[i]);
    }

    return string;
}

// Can't copy more than MAXLEN
// Do not edit string
char *CopyTillChar(char *string, int c, size_t maxlen)
{
    char *begin, *end;
    int len;

    begin = string;
    end = strchr(begin, c);
    len = (end - begin);

    if (len < 0 || len > maxlen)
        return NULL;

    return alloc_n_cpy(begin, len);
}

// Can't copy more than MAXLEN
// edit P
char *CopyTillChar2(char **p, int c, size_t maxlen)
{
    char *begin;
    int len;

    begin = *p;
    *p = strchr(begin, c);
    len = (*p - begin);

    if (len < 0 || len > maxlen)
        return NULL;

    return alloc_n_cpy(begin, len);
}

int char_count(const char *s, char c)
{
    char *p = (char *)s;
    int count = 0;

    do
    {
        if (*p == c)
            ++count;
    }while (*(p++));

    return count;
}

char *char_replace(const char *s, char from, char to)
{
    char *str;
    int len, i;

    len = strlen(s);
    str = calloc(len+1, sizeof(char));
    strncpy(str, s, len);
    str[len] = 0;

    for (i=0 ; i<len ; i++)
    {
        if (str[i] == from)
        {
            str[i] = to;
        }
    }

    return str;
}

// Make a function that supress repeated-occurence in a string array

regex_t regex_http_url;

bool RegexInit(void)
{
    char error[BUFSIZ];
    int err = regcomp(&regex_http_url, "^[A-Z]{3,7}[[:space:]]\\/[a-zA-Z0-9\\/\\._-]*((\\?[a-zA-Z0-9_]+\\=([[:alnum:][:punct:]_\\+\\-]+)?)(\\&[a-zA-Z0-9_]+\\=([[:alnum:][:punct:]_\\+\\-]+)?)*)?[[:space:]]HTTP\\/(1|1\\.1|2|3)$", REG_EXTENDED);
    if (err)
    {
        regerror(err, &regex_http_url, error, sizeof(error));
        printf("%s:%d : Error : %s\n", __FILE__, __LINE__, error);
        return false;
    }

    return true;
}

void RegexFree(void)
{
    regfree(&regex_http_url);
}

bool RegexMatch_HTTP_Url(const char *http_url)
{
    if (!http_url)
    {
        printf("%s:%d : Error : http_url is null.\n", __FILE__, __LINE__);
        return false;
    }

    int ret;
    char error[BUFSIZ];

    ret = regexec(&regex_http_url, http_url, 0, NULL, 0);
    if (!ret)
    {
        return true;
    }
    if (ret == REG_NOMATCH)
    {
        return false;
    }
    regerror(ret, &regex_http_url, error, sizeof(error));
    printf("%s:%d : Error : %s\n", __FILE__, __LINE__, error);

    return false;
}