### Prerequisite :

Nothing

***

### Compilation command :

```
gcc main.c dll/*.c useful/*.c fsm/*.c error/*.c server/client/http/*.c server/client/http/request/parse/*.c server/client/http/request/parse/header/*.c server/client/http/request/parse/header/radix_tree/*.c server/client/http/request/parse/method/*.c server/client/http/request/parse/request/*.c server/client/http/request/process/resource/*.c server/client/http/request/process/header/*.c server/client/http/request/process/*.c server/client/http/response/status_line/*.c server/client/http/response/*.c server/client/http/status_code/*.c server/client/http/mime/*.c server/option/*.c server/client/*.c server/logs/*.c server/*.c -Wall -o lightweighttpd
```
