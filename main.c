// std includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <errno.h>

// socket includes
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>

// my libs
#include "server/server.h"
#include "dll/dll.h"
#include "useful/useful.h"

#define PORT 5050
#define MAXWEIGHT 1024

void avanceP(char *p)
{
    ++p;
}

int main(int argc, char *argv[])
{
    printf("pid = %d\n", getpid());

    SERVER_Launch(argc, argv);

    return 0;
}
