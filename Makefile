BIN=lightweighttpd
SOURCES=$(shell find . -type f -regex ".*\.c")
OPTION=--disable-logs

$(BIN): $(SOURCES)
	gcc \
	main.c dll/*.c useful/*.c fsm/*.c error/*.c \
	server/client/http/*.c server/client/http/process/*.c server/client/http/request/*.c server/client/http/request/header/*.c server/client/http/request/header/radix_tree/*.c server/client/http/response/*.c server/client/http/mime/*.c server/option/*.c server/client/*.c server/logs/*.c server/*.c -Wall \
	-o $(BIN)

run: $(BIN)
	valgrind --leak-check=full --show-leak-kinds=all ./lightweighttpd $(OPTION)

clean:
	rm $(BIN)