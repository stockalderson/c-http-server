#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "../useful/useful.h"

#include "error.h"

lwd_error_e lwd_error = LWD_ERROR_OK;
lwd_status_code_e http_status = OK;

char *str_status_code(lwd_status_code_e status_code)
{
    char *status_code_s = NULL;
    int len;

    switch (status_code)
    {

        // 100
        case CONTINUE:
            len = strlen("CONTINUE");
            status_code_s = alloc_n_cpy("CONTINUE", len);
            break;

        // 101
        case SWITCHING_PROTOCOL:
            len = strlen("SWITCHING PROTOCOL");
            status_code_s = alloc_n_cpy("SWITCHING PROTOCOL", len);
            break;

        // 102
        case PROCESSING:
            len = strlen("PROCESSING");
            status_code_s = alloc_n_cpy("PROCESSING", len);
            break;

        // 103
        case EARLY_HINTS:
            len = strlen("EARLY HINTS");
            status_code_s = alloc_n_cpy("EARLY HINTS", len);
            break;

        // 200
        case OK:
            len = strlen("OK");
            status_code_s = alloc_n_cpy("OK", len);
            break;

        // 201
        case CREATED:
            len = strlen("CREATED");
            status_code_s = alloc_n_cpy("CREATED", len);
            break;

        // 202
        case ACCEPTED:
            len = strlen("ACCEPTED");
            status_code_s = alloc_n_cpy("ACCEPTED", len);
            break;

        // 203
        case NON_AUTORITATIVE_INFO:
            len = strlen("NON AUTORITATIVE INFO");
            status_code_s = alloc_n_cpy("NON AUTORITATIVE INFO", len);
            break;

        // 204
        case NO_CONTENT:
            len = strlen("NO CONTENT");
            status_code_s = alloc_n_cpy("NO CONTENT", len);
            break;

        // 205
        case RESET_CONTENT:
            len = strlen("RESET CONTENT");
            status_code_s = alloc_n_cpy("RESET CONTENT", len);
            break;

        // 206
        case PARTIAL_CONTENT:
            len = strlen("PARTIAL CONTENT");
            status_code_s = alloc_n_cpy("PARTIAL CONTENT", len);
            break;

        // 207
        case MULTI_STATUS:
            len = strlen("MULTI STATUS");
            status_code_s = alloc_n_cpy("MULTI STATUS", len);
            break;

        // 208
        case ALREADY_REPORTED:
            len = strlen("ALREADY REPORTED");
            status_code_s = alloc_n_cpy("ALREADY REPORTED", len);
            break;

        // 226
        case IM_USED:
            len = strlen("IM USED");
            status_code_s = alloc_n_cpy("IM USED", len);
            break;

        // 300
        case MULTIPLE_CHOICE:
            len = strlen("MULTIPLE CHOICE");
            status_code_s = alloc_n_cpy("MULTIPLE CHOICE", len);
            break;

        // 301
        case MOVE_PERMANENTLY:
            len = strlen("MOVE PERMANENTLY");
            status_code_s = alloc_n_cpy("MOVE PERMANENTLY", len);
            break;

        // 302
        case FOUND:
            len = strlen("FOUND");
            status_code_s = alloc_n_cpy("FOUND", len);
            break;

        // 303
        case SEE_OTHER:
            len = strlen("SEE OTHER");
            status_code_s = alloc_n_cpy("SEE OTHER", len);
            break;

        // 304
        case NOT_MODIFIED:
            len = strlen("NOT MODIFIED");
            status_code_s = alloc_n_cpy("NOT MODIFIED", len);
            break;

        // 305
        case USE_PROXY:
            len = strlen("USE PROXY");
            status_code_s = alloc_n_cpy("USE PROXY", len);
            break;

        // 306
        case UNUSED:
            len = strlen("UNUSED");
            status_code_s = alloc_n_cpy("UNUSED", len);
            break;

        // 307
        case TEMPORARY_REDIRECT:
            len = strlen("TEMPORARY REDIRECT");
            status_code_s = alloc_n_cpy("TEMPORARY REDIRECT", len);
            break;

        // 308
        case PERMANENT_REDIRECT:
            len = strlen("PERMANENT REDIRECT");
            status_code_s = alloc_n_cpy("PERMANENT REDIRECT", len);
            break;

        // 400
        case BAD_REQUEST:
            len = strlen("BAD REQUEST");
            status_code_s = alloc_n_cpy("BAD REQUEST", len);
            break;

        // 401
        case UNAUTHORIZED:
            len = strlen("UNAUTHORIZED");
            status_code_s = alloc_n_cpy("UNAUTHORIZED", len);
            break;

        // 402
        case PAYMENT_REQUIRED:
            len = strlen("PAYMENT REQUIRED");
            status_code_s = alloc_n_cpy("PAYMENT REQUIRED", len);
            break;

        // 403
        case FORBIDDEN:
            len = strlen("FORBIDDEN");
            status_code_s = alloc_n_cpy("FORBIDDEN", len);
            break;

        // 404
        case NOT_FOUND:
            len = strlen("NOT FOUND");
            status_code_s = alloc_n_cpy("NOT FOUND", len);
            break;

        // 405
        case METHOD_NOT_ALLOWED:
            len = strlen("METHOD NOT ALLOWED");
            status_code_s = alloc_n_cpy("METHOD NOT ALLOWED", len);
            break;

        // 406
        case NOT_ACCEPTABLE:
            len = strlen("NOT ACCEPTABLE");
            status_code_s = alloc_n_cpy("NOT ACCEPTABLE", len);
            break;

        // 407
        case PROXY_AUTHENTICATION_REQUIRED:
            len = strlen("PROXY AUTHENTICATION REQUIRED");
            status_code_s = alloc_n_cpy("PROXY AUTHENTICATION REQUIRED", len);
            break;
        
        // 408
        case REQUEST_TIMEOUT:
            len = strlen("REQUEST TIMEOUT");
            status_code_s = alloc_n_cpy("REQUEST TIMEOUT", len);
            break;

        // 409
        case CONFLICT:
            len = strlen("CONFLICT");
            status_code_s = alloc_n_cpy("CONFLICT", len);
            break;

        // 410
        case GONE:
            len = strlen("GONE");
            status_code_s = alloc_n_cpy("GONE", len);
            break;

        // 411
        case LENGTH_REQUIRED:
            len = strlen("LENGTH REQUIRED");
            status_code_s = alloc_n_cpy("LENGTH REQUIRED", len);
            break;

        // 412
        case PRECONDITION_FAILED:
            len = strlen("PRECONDITION FAILED");
            status_code_s = alloc_n_cpy("PRECONDITION FAILED", len);
            break;

        // 413
        case PAYLOAD_TOO_LARGE:
            len = strlen("PAYLOAD TOO LARGE");
            status_code_s = alloc_n_cpy("PAYLOAD TOO LARGE", len);
            break;

        // 414
        case URI_TOO_LONG:
            len = strlen("URI TOO LONG");
            status_code_s = alloc_n_cpy("URI TOO LONG", len);
            break;

        // 415
        case UNSUPPORTED_MEDIA_TYPE:
            len = strlen("UNSUPPORTED MEDIA TYPE");
            status_code_s = alloc_n_cpy("UNSUPPORTED MEDIA TYPE", len);
            break;

        // 416
        case RANGE_NOT_SATISFIABLE:
            len = strlen("RANGE NOT SATISFIABLE");
            status_code_s = alloc_n_cpy("RANGE NOT SATISFIABLE", len);
            break;

        // 417
        case EXPECTATION_FAILED:
            len = strlen("EXPECTATION FAILED");
            status_code_s = alloc_n_cpy("EXPECTATION FAILED", len);
            break;

        // 418
        case IM_TEAPOT:
            len = strlen("IM TEAPOT");
            status_code_s = alloc_n_cpy("IM TEAPOT", len);
            break;

        // 421
        case MISDIRECTED_REQUEST:
            len = strlen("MISDIRECTED REQUEST");
            status_code_s = alloc_n_cpy("MISDIRECTED REQUEST", len);
            break;

        // 422
        case UNPROCESSABLE_ENTITY:
            len = strlen("UNPROCESSABLE ENTITY");
            status_code_s = alloc_n_cpy("UNPROCESSABLE ENTITY", len);
            break;

        // 423
        case LOCKED:
            len = strlen("LOCKED");
            status_code_s = alloc_n_cpy("LOCKED", len);
            break;

        // 424
        case FAILED_DEPENDENCY:
            len = strlen("FAILED DEPENDENCY");
            status_code_s = alloc_n_cpy("FAILED DEPENDENCY", len);
            break;

        // 425
        case TOO_EARLY:
            len = strlen("TOO EARLY");
            status_code_s = alloc_n_cpy("TOO EARLY", len);
            break;

        // 426
        case UPGRADE_REQUIRED:
            len = strlen("UPGRADE REQUIRED");
            status_code_s = alloc_n_cpy("UPGRADE REQUIRED", len);
            break;

        // 428
        case PRECONDITION_REQUIRED:
            len = strlen("PRECONDITION REQUIRED");
            status_code_s = alloc_n_cpy("PRECONDITION REQUIRED", len);
            break;

        // 429
        case TOO_MANY_REQUESTS:
            len = strlen("TOO MANY REQUESTS");
            status_code_s = alloc_n_cpy("TOO MANY REQUESTS", len);
            break;

        // 431
        case REQUEST_HEADER_FIELDS_TOO_LARGE:
            len = strlen("REQUEST HEADER FIELDS TOO LARGE");
            status_code_s = alloc_n_cpy("REQUEST HEADER FIELDS TOO LARGE", len);
            break;

        // 451
        case UNAVAILABLE_FOR_LEGAL_REASONS:
            len = strlen("UNAVAILABLE FOR LEGAL REASONS");
            status_code_s = alloc_n_cpy("UNAVAILABLE FOR LEGAL REASONS", len);
            break;

        // 500
        case INTERNAL_SERVER_ERROR:
            len = strlen("INTERNAL SERVER ERROR");
            status_code_s = alloc_n_cpy("INTERNAL SERVER ERROR", len);
            break;

        // 501
        case NOT_IMPLEMENTED:
            len = strlen("NOT IMPLEMENTED");
            status_code_s = alloc_n_cpy("NOT IMPLEMENTED", len);
            break;

        // 502
        case BAD_GATEWAY:
            len = strlen("BAD GATEWAY");
            status_code_s = alloc_n_cpy("BAD GATEWAY", len);
            break;

        // 503
        case SERVICE_UNAVAILABLE:
            len = strlen("SERVICE UNAVAILABLE");
            status_code_s = alloc_n_cpy("SERVICE UNAVAILABLE", len);
            break;

        // 504
        case GATEWAY_TIMEOUT:
            len = strlen("GATEWAY TIMEOUT");
            status_code_s = alloc_n_cpy("GATEWAY TIMEOUT", len);
            break;

        // 505
        case HTTP_VERSION_NOT_SUPPORTED:
            len = strlen("HTTP VERSION NOT SUPPORTED");
            status_code_s = alloc_n_cpy("HTTP VERSION NOT SUPPORTED", len);
            break;

        // 506
        case VARIANT_ALSO_NEGOCIATES:
            len = strlen("VARIANT ALSO NEGOCIATES");
            status_code_s = alloc_n_cpy("VARIANT ALSO NEGOCIATES", len);
            break;

        // 507
        case INSUFFICIENT_STORAGE:
            len = strlen("INSUFFICIENT STORAGE");
            status_code_s = alloc_n_cpy("INSUFFICIENT STORAGE", len);
            break;

        // 508
        case LOOP_DETECTED:
            len = strlen("LOOP DETECTED");
            status_code_s = alloc_n_cpy("LOOP DETECTED", len);
            break;

        // 510
        case NOT_EXTENDED:
            len = strlen("NOT EXTENDED");
            status_code_s = alloc_n_cpy("NOT EXTENDED", len);
            break;

        // 511
        case NETWORK_AUTHENTICATION_REQUIRED:
            len = strlen("NETWORK AUTHENTICATION REQUIRED");
            status_code_s = alloc_n_cpy("NETWORK AUTHENTICATION REQUIRED", len);
            break;

        // Default set to 500 INTERNAL SERVER ERROR
        default:
            len = strlen("INTERNAL SERVER ERROR\0");
            status_code_s = alloc_n_cpy("INTERNAL SERVER ERROR", len);
            break;
    }

    return status_code_s;
}

char *str_status_code_free(char *status_code_s)
{
    if (status_code_s == NULL)
    {
        return NULL;
    }

    free(status_code_s);
    return NULL;
}