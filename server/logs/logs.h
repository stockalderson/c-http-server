#ifndef __LWD_LOGS_H__
#define __LWD_LOGS_H__

#include <stdio.h>
#include <sys/types.h>
#include <netinet/in.h>

#include "../../error/error.h"

void LOGS_Init(void);
void LOGS_Free(void);
int LOGS_Update(lwd_status_code_e status_code, struct sockaddr_in client_addr);

#endif
