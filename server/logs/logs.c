#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <arpa/inet.h>

#include "logs.h"

#define LOG_PATH "/server/logs/ip_log.txt"
#define UTC 1

typedef struct {
    char *path;
    char *buf;
    FILE *fd;
} lwd_logs_context_t;

static lwd_logs_context_t ctx;
static bool init = false; // Used to verify if context is already initialized or not

static int InitContext(void);
static void FreeContext(void);

static int InitContext(void)
{
    char *cur_dir = getcwd(NULL, 0);
    int16_t length = strlen(cur_dir) + strlen(LOG_PATH);
    ctx.path = calloc(length+1, sizeof(char));
    snprintf(ctx.path, length+1, "%s%s", cur_dir, LOG_PATH);
    free(cur_dir);

    // Test if file exists
    if (!access(ctx.path, F_OK) == 0)
    {
        printf("Log path : %s doesn't exists.\n", ctx.path);
        free(ctx.path);
        return 0;
    }

    init = true;
    return 1;
}

static void FreeContext(void)
{
    if (!init)
        return;
    free(ctx.path);
    init = false;
    
    return;
}

void LOGS_Init(void)
{
    if (init)
        return;

    if (!InitContext())
    {
        printf("Could not init logs context.\n");
        return;
    }
}

int LOGS_Update(lwd_status_code_e status_code, struct sockaddr_in client_addr)
{
    /* Declaration */

    struct in_addr client_ip_addr;
    time_t t;
    struct tm tm;
    char *status_phrase;

    /* Initialisation */

    client_ip_addr.s_addr = client_addr.sin_addr.s_addr;
    t = time(NULL);
    tm = *localtime(&t);
    status_phrase = str_status_code(status_code);

    /* Action */

    ctx.fd = fopen(ctx.path, "w");
    if (fprintf(ctx.fd, "%s:%d the %d/%02d/%d at %02d:%02d:%02d | %d %s\n", inet_ntoa(client_ip_addr), client_addr.sin_port, tm.tm_mday, tm.tm_mon+1, tm.tm_year+1900, tm.tm_hour+UTC, tm.tm_min, tm.tm_sec, status_code, status_phrase) < 0)
    {
        printf("%s:%d : fprintf() failed : %s\n", __FILE__, __LINE__, strerror(errno));
        fclose(ctx.fd);
        status_phrase = str_status_code_free(status_phrase);
        return 0;
    }
    status_phrase = str_status_code_free(status_phrase);
    fclose(ctx.fd);

    return 1;
}

void LOGS_Free(void)
{
    FreeContext();
}