#ifndef __LWD_OPTION_H__
#define __LWD_OPTION_H__

// Options
#define OPT_HELP "-h"
#define OPT_LONG_HELP "--help"
#define HELP_PRINT "Usage: lightweighttpd [OPTION]\n"\
                   "\n"\
                   "Options:\n"\
                   "    -h, --help:\n"\
                   "            print this message.\n\n"\
                   "    --disable-logs:\n"\
                   "            don't write logs.\n\n"\

#define OPT_D_LOGS "--disable-logs"

#include <stdbool.h>

typedef struct {
    bool disable_logs;
    bool help;
} lwd_option_t;

lwd_option_t *OPTION_New(void);
void OPTION_Free(lwd_option_t *opt);
void OPTION_Init(lwd_option_t *opt);
bool OPTION_Check(lwd_option_t *opt, char *argv[]);

#endif