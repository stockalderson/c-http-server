#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "option.h"

lwd_option_t *OPTION_New(void)
{
    return (lwd_option_t *)calloc(1, sizeof(lwd_option_t));
}

void OPTION_Free(lwd_option_t *opt)
{
    if (opt)
        free(opt);

    return;
}

void OPTION_Init(lwd_option_t *opt)
{
    opt->disable_logs = false;
    opt->help = false;
}

// Return 0 to stop and 1 to continue
bool OPTION_Check(lwd_option_t *opt, char *argv[])
{
    if (!opt || argv == NULL || *argv == NULL)
    {
        printf("%s:%d : error.\n", __FILE__, __LINE__);
        return false;
    }

    for (int i=1 ; argv[i] != NULL ; i++)
    {
        // Options that return 
        if ((strcmp(argv[i], OPT_HELP) == 0) || (strcmp(argv[i], OPT_LONG_HELP) == 0))
        {
            opt->help = true;
            printf(HELP_PRINT);
            return false;
        }
        else if (strcmp(argv[i], OPT_D_LOGS) == 0)
        {
            opt->disable_logs = true;
        }
        else
        {
            printf("Unknown option.\n\n");
            printf(HELP_PRINT);
            return false;
        }
    }

    return true;
}