#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <netinet/tcp.h>
#include <fcntl.h>
#include <time.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include "../useful/useful.h"
#include "../error/error.h"

#include "server.h"
#include "client/client.h"
#include "logs/logs.h"
#include "client/http/request/header/header.h"

#define PORT 5050
#define RECV_TIMEOUT 500000UL
#define WEBSITE_ROOT "/site/www"
// #define WWWROOT "/home/zecksidd/Documents/Code/Network/html\0"
#define WWWROOT ""

static int srv_sockfd = -1;

static lwd_server_t *SERVER_Init(void);
static lwd_server_t *SERVER_Free(lwd_server_t *server);
static int SERVER_InitSocket(lwd_server_t *server);
static bool SERVER_SetOptions(lwd_server_t *server, int argc, char *argv[]);
static int SERVER_Start(lwd_server_t *server);

void sigintHandler(int sig_num)
{
    printf("\nQuitting by Ctrl+C\n");

    lwd_error = LWD_ERROR_INTERRUPT;

    shutdown(srv_sockfd, SHUT_RDWR);
    close(srv_sockfd);
}

void sigpipeHandler(int sig_num)
{
    printf("sigpipe error : %d\n", sig_num);
}

static lwd_server_t *SERVER_Init(void)
{
    if (!InitHeaderTree())
    {
        printf("%s:%d : Error : InitHeaderTree() failed.\n", __FILE__, __LINE__);
        return NULL;
    }

    // Regex
    if (!RegexInit())
    {
        printf("%s:%d : Error : RegexInit() failed.\n", __FILE__, __LINE__);
        return NULL;
    }

    signal(SIGINT, sigintHandler);
    signal(SIGPIPE, sigpipeHandler);

    lwd_server_t *server = calloc(1, sizeof(lwd_server_t));

    const char wwwroot[] = WWWROOT;
    char *pwwwroot = NULL;
    int wwwroot_len = strlen(wwwroot);
    if (!wwwroot_len)
    {
        // Get webserver root
        if (!(pwwwroot = getcwd(NULL, 0)))
        {
            printf("%s:%d : getcwd() failed : %s\n", __FILE__, __LINE__, strerror(errno));
            SERVER_Free(server);
            return NULL;
        }
        wwwroot_len = strlen(pwwwroot) + strlen(WEBSITE_ROOT);
        // Alloc exact size of website root
        server->wwwroot = (char *)calloc(wwwroot_len+1, sizeof(char));
        // Copy website root to wwwroot
        strcpy(server->wwwroot, pwwwroot);
        strcat(server->wwwroot, WEBSITE_ROOT);
        server->wwwroot[wwwroot_len] = 0;
        free(pwwwroot);
    }else{
        server->wwwroot = (char *)calloc(wwwroot_len+1, sizeof(char));
        strncpy(server->wwwroot, wwwroot, wwwroot_len);
        server->wwwroot[wwwroot_len] = 0;
    }
    printf("wwwroot = %s\n", server->wwwroot);

    server->addr.sin_family = AF_INET;
    server->addr.sin_addr.s_addr = INADDR_ANY;
    server->addr.sin_port = htons(PORT);

    return server;
}

static int SERVER_InitSocket(lwd_server_t *server)
{

    /* Declaration */
    
    int tcp_nodelay, recv_timeout, reuse_addr;

    /* Initialisation */

    tcp_nodelay = 1;
    recv_timeout = RECV_TIMEOUT;
    reuse_addr = 1;

    srv_sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (srv_sockfd == -1)
    {
        printf("%s:%d : socket() failed : %s\n", __FILE__, __LINE__, strerror(errno));
        goto FatalError;
    }

    // set TCP_NODELAY for imediately free buffer
    setsockopt(srv_sockfd, IPPROTO_TCP, TCP_NODELAY, (void *)&tcp_nodelay, sizeof(tcp_nodelay));

    // Set RECV Timeout
    setsockopt(srv_sockfd, SOL_SOCKET, SO_RCVTIMEO, (void *)&recv_timeout, sizeof(recv_timeout));

    // Reuse this addr anyway
    setsockopt(srv_sockfd, SOL_SOCKET, SO_REUSEADDR, &reuse_addr, sizeof(reuse_addr));

    // bind address to socket file descriptor
    if (bind(srv_sockfd, (struct sockaddr *)&server->addr, sizeof(server->addr)) != 0)
    {
        printf("%s:%d : bind() failed : %s\n", __FILE__, __LINE__, strerror(errno));
        goto FatalError;
    }

    // Create Listener
    if (listen(srv_sockfd, MAX_CONNECTION) != 0)
    {
        printf("%s:%d : listen() failed : %s\n", __FILE__, __LINE__, strerror(errno));
        goto FatalError;
    }

    return 1;

FatalError:

    if (srv_sockfd >= 0)
    {
        shutdown(srv_sockfd, SHUT_RDWR);
        close(srv_sockfd);
    }

    return 0;
}

static lwd_server_t *SERVER_Free(lwd_server_t *server)
{
    RegexFree();
    FreeHeaderTree();
    LOGS_Free();

    if (!server)
        return NULL;
    if (srv_sockfd > 0)
    {
        shutdown(srv_sockfd, O_RDWR);
        close(srv_sockfd);
    }
    if (server->wwwroot)
        free(server->wwwroot);
    free(server);

    return NULL;
}

static bool SERVER_SetOptions(lwd_server_t *server, int argc, char *argv[])
{
    if (argc <= 1)
        return true;
    if (argv == NULL || *argv == NULL)
        return false;

    OPTION_Init(&server->option);

    if (!OPTION_Check(&server->option, argv))
        return 0;

    return 1;
}

static int SERVER_Start(lwd_server_t *server)
{
    int run = 1;

    LOGS_Init();

    printf("------------------------------------------------\n");
    printf("Listening on port : %d\n\n", ntohs(server->addr.sin_port));

    // Main loop
    while (run)
    {
        if (!CLIENT_NewConnection(srv_sockfd, server->wwwroot))
        {
            if (lwd_error == LWD_ERROR_INTERRUPT)
                goto Interrupt;
            goto FatalError;
        }
    }

Interrupt:
    return 1;

FatalError:
    return 0;
}

void SERVER_Launch(int argc, char *argv[])
{
    lwd_server_t *server = SERVER_Init();
    if (!server)
    {
        printf("%s:%d : Error : SERVER_Init() failed.\n", __FILE__, __LINE__);
        goto FatalError;
    }
    if (!SERVER_SetOptions(server, argc, argv))
    {
        // Case option is : -h/--help
        if (server->option.help)
        {
            SERVER_Free(server);
            exit(0);
        }
        printf("%s:%d : Error : SERVER_SetOptions() failed.\n", __FILE__, __LINE__);
        goto FatalError;
    }
    if (!SERVER_InitSocket(server))
    {
        printf("SERVER_InitSocket() failed.\n");
        goto FatalError;
    }

    if (!SERVER_Start(server))
    {
        printf("SERVER_Start() failed.\n");
        goto FatalError;
    }

    SERVER_Free(server);
    printf("Server terminated correctly.\n");
    return;

FatalError:
    SERVER_Free(server);
    printf("Server terminated with an error.\n");
    return;
}