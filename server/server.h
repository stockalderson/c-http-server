#ifndef __LWD_SERVER_H__
#define __LWD_SERVER_H__

#define MAX_CONNECTION 5
#define HEADER_LEN 1024
#define ROOT_SITE "/site/www"

#include <netinet/in.h>
#include <stdbool.h>

#include "option/option.h"

typedef struct {
    struct sockaddr_in addr;
    int addr_len;
    char *wwwroot;
    lwd_option_t option;
} lwd_server_t;

void SERVER_Launch(int argc, char *argv[]);

#endif