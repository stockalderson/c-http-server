#ifndef __LWD_HTTP_H__
#define __LWD_HTTP_H__

#include <stdbool.h>
#include "../../../error/error.h"

#include "response/response.h"
#include "request/request.h"

typedef struct {
    lwd_request_t *request;
    lwd_response_t *response;
} lwd_http_t;

lwd_http_t *HTTP_New(void);
lwd_http_t *HTTP_Free(lwd_http_t *http);
bool HTTP_IsTLS(char *request);
bool HTTP_HandleTCPHandshake(char *request, int rsockfd);
bool HTTP_ParseRequest(lwd_http_t *http, char *request);
bool HTTP_ProcessRequest(lwd_http_t *http, const char *wwwroot);
bool HTTP_GenerateResponse(lwd_http_t *http);
bool HTTP_SendResponse(lwd_http_t *http, int writefd);
void HTTP_ErrorPage(lwd_http_t *http, int rsockfd);

#endif