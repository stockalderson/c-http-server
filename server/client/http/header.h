#ifndef __LWD_HEADER_H__
#define __LWD_HEADER_H__

#include "../../../dll/dll.h"

#define STR_CONTENT_TYPE "Content-Type"
#define STR_CONTENT_LENGTH "Content-Length"
#define STR_DATE "Date"

#define VALUE(item) ((item) ? ((lwd_header_t *)item->value) : NULL)

typedef enum {
    HEADER_ERROR,                               // 0
    HEADER_A_IM,                                // 1
    HEADER_ACCEPT,                              // 2
    HEADER_ACCEPT_CHARSET,                      // 3
    HEADER_ACCEPT_DATETIME,                     // 4
    HEADER_ACCEPT_ENCODING,                     // 5
    HEADER_ACCEPT_LANGUAGE,                     // 6
    HEADER_ACCESS_CONTROL_REQUEST_HEADERS,      // 7
    HEADER_ACCESS_CONTROL_REQUEST_METHOD,       // 8
    HEADER_AUTHORIZATION,                       // 9
    HEADER_CACHE_CONTROL,                       // 10
    HEADER_CONNECTION,                          // 11
    HEADER_CONTENT_LENGTH,                      // 12
    HEADER_CONTENT_TYPE,                        // 13
    HEADER_COOKIE,                              // 14
    HEADER_DATE,                                // 15
    HEADER_DNT,                                 // 16
    HEADER_EXPECT,                              // 17
    HEADER_FORWARDED,                           // 18
    HEADER_FROM,                                // 19
    HEADER_HOST,                                // 20
    HEADER_IF_MODIFIED_SINCE,                   // 21
    HEADER_IF_MATCH,                            // 22
    HEADER_IF_NONE_MATCH,                       // 23
    HEADER_IF_RANGE,                            // 24
    HEADER_IF_UNMODIFIED_SINCE,                 // 25
    HEADER_MAX_FORWARDS,                        // 26
    HEADER_ORIGIN,                              // 27
    HEADER_PRAGMA,                              // 28
    HEADER_PROXY_AUTHORIZATION,                 // 29
    HEADER_RANGE,                               // 30
    HEADER_REFERER,                             // 31
    HEADER_SEC_CH_UA,                           // 32
    HEADER_SEC_CH_UA_ARCH,                      // 33
    HEADER_SEC_CH_UA_BITNESS,                   // 34
    HEADER_SEC_CH_UA_FULL_VERSION_LIST,         // 35
    HEADER_SEC_CH_UA_FULL_VERSION,              // 36
    HEADER_SEC_CH_UA_MOBILE,                    // 37
    HEADER_SEC_CH_UA_MODEL,                     // 38
    HEADER_SEC_CH_UA_PLATFORM_VERSION,          // 39
    HEADER_SEC_CH_UA_PLATFORM,                  // 40
    HEADER_SEC_FETCH_DEST,                      // 41
    HEADER_SEC_FETCH_MODE,                      // 42
    HEADER_SEC_FETCH_SITE,                      // 43
    HEADER_SEC_FETCH_USER,                      // 44
    HEADER_SEC_GPC,                             // 45
    HEADER_SEC_WEBSOCKET_ACCEPT,                // 46
    HEADER_TE,                                  // 47
    HEADER_UPGRADE,                             // 48
    HEADER_UPGRADE_INSECURE_REQUESTS,           // 49
    HEADER_USER_AGENT,                          // 50
    HEADER_VIA,                                 // 51
    HEADER_WARNING,                             // 52
} lwd_header_key_code_e;

typedef struct {
    char *key;
    char *value;
} lwd_header_t;

void HEADER_FreeHeaderList(lwd_list_t *list);

#endif