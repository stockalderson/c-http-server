#ifndef LWD_REQUEST_H
#define LWD_REQUEST_H

#include "../../../../dll/dll.h"
#include "../header.h"

typedef enum {
    M_GET,
    M_POST,
    M_PUT,
    M_PATCH,
    M_HEAD,
    M_OPTIONS,
    M_DELETE,
    M_CONNECT,
    M_TRACE,
    M_NOT_FOUND,
} lwd_method_e;

typedef struct {
    lwd_header_key_code_e key;
    char *value;
} lwd_request_header_t;

typedef struct {
    lwd_method_e method;
    char *uri;
    char *protocol;

    lwd_list_t *headers;
    lwd_list_t *variables;
} lwd_request_t;

lwd_request_t *REQUEST_ParseRequest(char *request);
void REQUEST_Free(lwd_request_t *r);

#endif