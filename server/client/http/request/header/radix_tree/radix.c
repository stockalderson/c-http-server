#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "radix.h"

Node *WalkDeepLeft(Node *node);
Node *WalkBackBranch(Node *node);
Node *BranchCompress(Node *begin, Node *end);
Node *FindNotViewed(Node *node, lwd_list_t *list);

Node *RADIX_NewNode(Node *parent, char *label, short int terminal)
{
    Node *new_node = (Node *)calloc(1, sizeof(Node));
    new_node->parent = parent;

    if (label == NULL)
    {
        new_node->label_len = 0;
        new_node->terminal = terminal;
        return new_node;
    }
    int l = strlen(label);
    char *cplabel = calloc(l+1, sizeof(char));
    strncpy(cplabel, label, l);
    cplabel[l] = 0;
    new_node->label = cplabel;
    new_node->label_len = l;
    new_node->terminal = terminal;

    return new_node;
}

void RADIX_FreeNode(Node *node)
{
    if (node == NULL)   
        return;

    if (node->label != NULL)
        free(node->label);

    free(node);

    return;
}

void RADIX_FreeBranch(Node *node)
{
    Item *child = RADIX_FIRST_CHILD(node);
    while (child)
    {
        RADIX_FreeBranch(RADIX_CHILD_NODE(child));
        child = RADIX_NEXT_CHILD(child);
    }
    DLL_Free(node->children, false);
    RADIX_FreeNode(node);

    return;
}

void RADIX_AddChild(Node *node, Node *child)
{
    if (node == NULL || child == NULL)
    {
        return;
    }

    if (node->children == NULL)
    {
        node->children = DLL_NewList();
    }

    DLL_AddRight(node->children, (void *)child);

    return;
}

Node *RADIX_FindChild(Node *node, char *label)
{
    if (node == NULL || label == NULL || node->children == NULL)
        return NULL;

    Item *child = RADIX_FIRST_CHILD(node);
    while (child)
    {
        if (strcmp(label, ((Node *)child->value)->label) == 0)
        {
            return RADIX_CHILD_NODE(child);
        }
        child = RADIX_NEXT_CHILD(child);
    }

    return NULL;
}

void RADIX_Print(Node *node, char *indent)
{
    if (node == NULL || indent == NULL)
        return;

    printf("%s+ %s, t = %d, value = %ld\n", indent, node->label, node->terminal, (size_t)node->value);

    char buf[strlen(indent)+3];
    sprintf(buf, "%s|  ", indent);

    lwd_list_t *children = node->children;
    if (children == NULL)
        return;

    Item *child = RADIX_FIRST_CHILD(node);
    while (child)
    {
        RADIX_Print(RADIX_CHILD_NODE(child), buf);
        child = RADIX_NEXT_CHILD(child);
    }

    return;
}

Node *WalkDeepLeft(Node *node)
{
    Item *child = RADIX_FIRST_CHILD(node);

    return child ? WalkDeepLeft(RADIX_CHILD_NODE(child)) : node;
}

Node *WalkBackBranch(Node *node)
{
    if (node == NULL)
        return NULL;

    Node *cur = node;
    while (cur && cur->parent != NULL)
    {
        size_t s = RADIX_FAMILY_SIZE(cur->parent);
        if (s != 1 || cur->parent->terminal)
        {
            return cur;
        }

        cur = cur->parent;
    }

    return NULL;
}

Node *BranchCompress(Node *begin, Node *end)
{
    if (begin == NULL || end == NULL || begin == end)
        return NULL;

    size_t bufsize = 0;

    Node *cur = begin;
    Node *to_delete = NULL;

    // Count total label length
    while (cur != end)
    {
        bufsize += cur->label_len;
        cur = RADIX_CHILD_NODE(RADIX_FIRST_CHILD(cur));
    }
    bufsize += cur->label_len;

    char *label = calloc(bufsize+1, sizeof(char));
    char *p = label;
    char c;
    char *x;

    // Concat labels into buffer
    cur = begin;
    while (cur != end)
    {
        x = cur->label;
        while ((c = *x++))
            *p++ = c;
        cur = RADIX_CHILD_NODE(RADIX_FIRST_CHILD(cur));
    }
    x = cur->label;
    while ((*p++ = *x++));

    // Apply compress
    RADIX_SetLabel(begin, label);
    free(label);
    begin->terminal = end->terminal;
    begin->value = end->value;
    to_delete = RADIX_CHILD_NODE(RADIX_FIRST_CHILD(begin));
    DLL_Free(begin->children, false);
    begin->children = end->children;
    end->children = NULL;
    RADIX_FreeBranch(to_delete);

    return begin;
}

void RADIX_SetLabel(Node *node, char *label)
{
    if (node == NULL || label == NULL)
        return;

    if (node->label != NULL)
        free(node->label);

    int len = strlen(label);
    node->label = calloc(len+1, sizeof(char));
    strncpy(node->label, label, len);
    node->label_len = len;

    return;
}

Node *FindNotViewed(Node *node, lwd_list_t *viewed)
{
    if (node == NULL || viewed == NULL)
        return NULL;

    Item *cur = RADIX_FIRST_CHILD(node);
    while (cur) 
    {
        if (DLL_Find(viewed, RADIX_CHILD_NODE(cur))) {
            cur = RADIX_NEXT_CHILD(cur);
            continue;
        }

        return RADIX_CHILD_NODE(cur);
    }

    return NULL;
}

void RADIX_Compress(Node *root)
{
    if (root == NULL)
        return;

    lwd_list_t *viewed_node = DLL_NewList();

    Node *cur = root;
    Node *start, *end, *new, *found;

    end = WalkDeepLeft(cur);
    while (end)
    {
        start = WalkBackBranch(end);
        if (start == NULL)
            break;
        
        if ((new = BranchCompress(start, end)) != NULL)
        {
            start = end = new;
            DLL_AddRight(viewed_node, new);
        }
        else
            DLL_AddRight(viewed_node, end);

        if ((found = FindNotViewed(end->parent, viewed_node)))
        {
            end = WalkDeepLeft(found);
            continue;
        }

        end = start->parent;
    }

    DLL_Free(viewed_node, false);

    return;
}

