#ifndef __RADIX_TREE_H__
#define __RADIX_TREE_H__

#include "../../../../../../dll/dll.h"

#define RADIX_FIRST_CHILD(node) ((node)->children ? (node)->children->begin : NULL)
#define RADIX_NEXT_CHILD(item) ((item) ? (item)->next : NULL)
#define RADIX_CHILD_NODE(item) ((item) ? (Node *)(item)->value : NULL)
#define RADIX_FAMILY_SIZE(node) DLL_Length((node)->children)

struct Node {
    struct Node *parent;
    lwd_list_t *children;
    char *label;
    size_t label_len;
    void *value;
    short int terminal;
};

typedef struct Node Node;

Node *RADIX_NewNode(Node *parent, char *label, short int terminal);
void RADIX_FreeNode(Node *node);
void RADIX_FreeBranch(Node *root);
void RADIX_AddChild(Node *node, Node *child);
Node *RADIX_FindChild(Node *node, char *label);
void RADIX_Compress(Node *root);
void RADIX_SetLabel(Node *node, char *label);
void RADIX_Print(Node *node, char *indent);

#endif