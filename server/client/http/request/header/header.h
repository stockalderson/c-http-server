#ifndef __LWD_PARSE_HEADER_H__
#define __LWD_PARSE_HEADER_H__

#include "../../../../../error/error.h"
#include "../../header.h"

typedef struct {
    char *header;
    lwd_header_key_code_e code;
}lwd_header_key_t;

typedef struct {
    lwd_header_key_code_e *keys;
    char **values;
    int num;
}lwd_parse_header_t;

/* Make request header struct */

int InitHeaderTree(void);
void FreeHeaderTree(void);

lwd_header_key_t *HEADER_KEY_New(void);
void HEADER_KEY_Free(lwd_header_key_t *header_key);
lwd_header_key_code_e HEADER_KEY_FindByStr(const char *header_key);

#endif