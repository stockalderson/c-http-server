#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../../../../../useful/useful.h"
#include "../../../../../fsm/fsm.h"
#include "../../../../../dll/dll.h"
#include "header.h"
#include "radix_tree/radix.h"

lwd_header_key_t ALLOWED_HEADER_KEYS[] = {
    {.header = "a-im", .code = HEADER_A_IM},
    {.header = "accept", .code = HEADER_ACCEPT},
    {.header = "accept-charset", .code = HEADER_ACCEPT_CHARSET},
    {.header = "accept-encoding", .code = HEADER_ACCEPT_ENCODING},
    {.header = "accept-language", .code = HEADER_ACCEPT_LANGUAGE},
    {.header = "accept-datetime", .code = HEADER_ACCEPT_DATETIME},
    {.header = "access-control-request-method", .code = HEADER_ACCESS_CONTROL_REQUEST_METHOD},
    {.header = "access-control-request-headers", .code = HEADER_ACCESS_CONTROL_REQUEST_HEADERS},
    {.header = "authorization", .code = HEADER_AUTHORIZATION},
    {.header = "cache-control", .code = HEADER_CACHE_CONTROL},
    {.header = "connection", .code = HEADER_CONNECTION},
    {.header = "content-length", .code = HEADER_CONTENT_LENGTH},
    {.header = "content-type", .code = HEADER_CONTENT_TYPE},
    {.header = "cookie", .code = HEADER_COOKIE},
    {.header = "date", .code = HEADER_DATE},
    {.header = "dnt", .code = HEADER_DNT},
    {.header = "expect", .code = HEADER_EXPECT},
    {.header = "forwarded", .code = HEADER_FORWARDED},
    {.header = "from", .code = HEADER_FROM},
    {.header = "host", .code = HEADER_HOST},
    {.header = "if-modified-since", .code = HEADER_IF_MODIFIED_SINCE},
    {.header = "if-match", .code = HEADER_IF_MATCH},
    {.header = "if-none-match", .code = HEADER_IF_NONE_MATCH},
    {.header = "if-range", .code = HEADER_IF_RANGE},
    {.header = "if-unmodified-since", .code = HEADER_IF_UNMODIFIED_SINCE},
    {.header = "max-forward", .code = HEADER_MAX_FORWARDS},
    {.header = "origin", .code = HEADER_ORIGIN},
    {.header = "pragma", .code = HEADER_PRAGMA},
    {.header = "proxy-authorization", .code = HEADER_PROXY_AUTHORIZATION},
    {.header = "range", .code = HEADER_RANGE},
    {.header = "referer", .code = HEADER_REFERER},
    {.header = "sec-ch-ua", .code = HEADER_SEC_CH_UA},
    {.header = "sec-ch-ua-arch", .code = HEADER_SEC_CH_UA_ARCH},
    {.header = "sec-ch-ua-bitness", .code = HEADER_SEC_CH_UA_BITNESS},
    {.header = "sec-ch-ua-full-version", .code = HEADER_SEC_CH_UA_FULL_VERSION},
    {.header = "sec-ch-ua-full-version-list", .code = HEADER_SEC_CH_UA_FULL_VERSION_LIST},
    {.header = "sec-ch-ua-mobile", .code = HEADER_SEC_CH_UA_MOBILE},
    {.header = "sec-ch-ua-model", .code = HEADER_SEC_CH_UA_MODEL},
    {.header = "sec-ch-ua-platform-version", .code = HEADER_SEC_CH_UA_PLATFORM_VERSION},
    {.header = "sec-ch-ua-platform", .code = HEADER_SEC_CH_UA_PLATFORM},
    {.header = "sec-fetch-dest", .code = HEADER_SEC_FETCH_DEST},
    {.header = "sec-fetch-mode", .code = HEADER_SEC_FETCH_MODE},
    {.header = "sec-fetch-site", .code = HEADER_SEC_FETCH_SITE},
    {.header = "sec-fetch-user", .code = HEADER_SEC_FETCH_USER},
    {.header = "sec-gpc", .code = HEADER_SEC_GPC},
    {.header = "sec-websocket-accept", .code = HEADER_SEC_WEBSOCKET_ACCEPT},
    {.header = "te", .code = HEADER_TE},
    {.header = "user-agent", .code = HEADER_USER_AGENT},
    {.header = "upgrade", .code = HEADER_UPGRADE},
    {.header = "upgrade-insecure-requests", .code = HEADER_UPGRADE_INSECURE_REQUESTS},
    {.header = "via", .code = HEADER_VIA},
    {.header = "warning", .code = HEADER_WARNING},
};

Node *root = NULL;

int InitHeaderTree(void)
{
    int i = 0;
    char c = 0;
    char buf[2];
    char *h = NULL;

    root = RADIX_NewNode(NULL, NULL, 1);
    Node *cur = root;
    Node *found = NULL;
    Node *new_node = NULL;

    for (i=0 ; i<sizeof(ALLOWED_HEADER_KEYS)/sizeof(lwd_header_key_t) ; i++)
    {
        h = ALLOWED_HEADER_KEYS[i].header;

        while ((c = *h++))
        {
            buf[0] = c;
            buf[1] = '\0';
            found = RADIX_FindChild(cur, buf);
            if (found == NULL)
            {
                new_node = RADIX_NewNode(cur, buf, 0);
                RADIX_AddChild(cur, new_node);
                cur = new_node;
            }else{
                cur = found;
            }
        }
        cur->terminal = 1;
        cur->value = (void *)((size_t)ALLOWED_HEADER_KEYS[i].code);
        cur = root;
    }
    RADIX_Compress(root);

    return 1;
}

void FreeHeaderTree(void)
{
    if (root != NULL)
    {
        RADIX_FreeBranch(root);
        root = NULL;
    }

    return;
}

lwd_header_key_t *HEADER_KEY_New(void)
{
    return (lwd_header_key_t *)calloc(1, sizeof(lwd_header_key_t));
}

void HEADER_KEY_Free(lwd_header_key_t *header_key)
{
    if (!header_key)
        return;

    if (header_key->header)
        free(header_key->header);

    free(header_key);

    return;
}

lwd_header_key_code_e HEADER_KEY_FindByStr(const char *header_key)
{
    if (!header_key)
        return HEADER_ERROR;
    
    lwd_header_key_code_e code = HEADER_ERROR;
    char *label = NULL;
    char *h = (char *)header_key;
    Node *cur = root;
    Item *item = RADIX_FIRST_CHILD(cur);
    short int label_len;

    lowercase(h);

    while (item)
    {
        label = RADIX_CHILD_NODE(item)->label;
        label_len = RADIX_CHILD_NODE(item)->label_len;
        
        if (strncmp(h, label, label_len))
        {
            item = RADIX_NEXT_CHILD(item);
            continue;
        }
        h += label_len;

        cur = RADIX_CHILD_NODE(item);
        if (*h == 0)
        {
            if (cur->terminal)
            {
                code = (lwd_header_key_code_e)cur->value;
                return code;
            }
            return HEADER_ERROR;
        }
        
        item = RADIX_FIRST_CHILD(cur);
    }

    return code;
}