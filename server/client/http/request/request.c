#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "request.h"
#include "header/header.h"
#include "../../../../useful/useful.h"
#include "../../../../error/error.h"

static lwd_method_e REQUEST_ParseMethod(char *status_line)
{
    char *method, *c;
    lwd_method_e method_e = M_NOT_FOUND;

    method = CopyTillChar(status_line, ' ', BUFSIZ);
    c = method;

    switch (*c)
    {
        case 'G':
            if (!strcmp(method, "GET"))
                method_e = M_GET;
            break;

        case 'P':
            ++c;
            switch (*c)
            {
                case 'O':
                    if (!strcmp(method, "POST"))
                        method_e = M_POST;
                    break;
            }
            break;
    }

    free(method);
    return method_e;
}

static char *REQUEST_ParseURI(char *status_line)
{
    char *uri;
    int len;
    char *p = status_line;

    p = strchr(p, ' ');
    ++p;

    if (strchr(p, '?'))
    {
        uri = CopyTillChar(p, '?', BUFSIZ);
        return uri;
    }
    uri = CopyTillChar(p, ' ', BUFSIZ);

    if (strcmp(uri, "/") == 0)
    {
        free(uri);
        len = strlen("/index.html");
        uri = calloc(len+1, sizeof(char));
        strncpy(uri, "/index.html", len);
        uri[len] = 0;
    }

    return uri;
}

static char *REQUEST_ParseProtocol(char *status_line)
{
    char *p = status_line;

    p = strchr(p, ' ');
    ++p;
    p = strchr(p, ' ');
    ++p;

    char *s = CopyTillChar(p, 0, 10);

    return s;
}

static bool REQUEST_ParseStatusLine(lwd_request_t *r, char *request)
{
    char *status_line;
    int len;

    status_line = strchr(request, '\r');
    len = status_line - request;
    if (len < 0)
    {
        http_status = BAD_REQUEST;
        goto Error;
    }
    status_line = calloc(len+1, sizeof(char));
    strncpy(status_line, request, len);
    status_line[len] = 0;

    if (!RegexMatch_HTTP_Url(status_line))
    {
        printf("Status line isn't correct. Bad request\n");
        http_status = BAD_REQUEST;
        goto Error;
    }

    r->method = REQUEST_ParseMethod(status_line);
    if (r->method == M_NOT_FOUND)
    {
        http_status = METHOD_NOT_ALLOWED;
        goto Error;
    }
    r->uri = REQUEST_ParseURI(status_line);
    r->protocol = REQUEST_ParseProtocol(status_line);
    if (strcmp(r->protocol, "HTTP/1.1") != 0)
    {
        http_status = INTERNAL_SERVER_ERROR; // Chercher la bonne erreur
        goto Error;
    }
    free(status_line);

    return true;

Error:
    if (status_line)
        free(status_line);
    return false;
}

static lwd_request_header_t *REQUEST_ParseHeader(char *header)
{
    size_t len;
    char *p, *s_key;
    lwd_request_header_t *key;

    key = calloc(1, sizeof(lwd_request_header_t));

    p = header;
    s_key = strchr(p, ':');
    if (!s_key) // Character not found
    {
        free(key);
        return NULL;
    }

    len = s_key - p;
    s_key = calloc(len+1, sizeof(char));
    strncpy(s_key, p, len);
    
    // Search for corresponding header
    key->key = HEADER_KEY_FindByStr(s_key);
    free(s_key);

    // Copy value
    p = strchr(p, ':');
    p+=2; // Jump semi-colon and space
    len = strlen(header) - (len+1);
    key->value = calloc(len+1, sizeof(char));
    strncpy(key->value, p, len);

    return key;
}

static void freeKey(Item *item)
{
    if (!item->value)
        return;
    lwd_request_header_t *key = (lwd_request_header_t *)item->value;

    if (key->value)
        free(key->value);
    free(key);
}

static void REQUEST_FreeHeadersList(lwd_request_t *r)
{
    if (!r->headers)
        return;

    DLL_Free(r->headers, freeKey);
}

/* 
    Return pointer to the isolated header
*/
static char *REQUEST_IsolateHeader(char *header)
{
    int hlen;
    char *tmp, *h;

    h = header;
    tmp = h;
    while ((tmp = strchr(h, '\r')))
    {
        if (*(tmp+2) == ' ' || *(tmp+2) == '\t')
            continue;
        break;
    }
    hlen = tmp - h;
    tmp = h;
    h = calloc(hlen+1, sizeof(char));
    strncpy(h, tmp, hlen); // Copy header line into h
    h[hlen] = 0;

    return h;
}

typedef enum {
    STATE_WRITE,
    STATE_LWSP,
    STATE_CRLF,
    STATE_FINISH,
} unfold_state;

/* 
    Return pointer to the unfolded header
*/
static char *REQUEST_UnfoldHeader(char *header)
{
    int i, j, len = strlen(header);
    char *unfold, *c, *tmp;
    char buf[len+1];
    memset(buf, 0, len+1);
    unfold_state state;

    c = header;
    i = j = 0;

    state = STATE_WRITE;
    while (state != STATE_FINISH)
    {
        switch (state)
        {
            case STATE_WRITE:
                for (; *c ; ++c)
                {
                    if (*c == ' ' || *c == '\t')
                    {
                        state = STATE_LWSP;
                        break;
                    }
                    if (*c == '\r')
                    {
                        state = STATE_CRLF;
                        break;
                    }

                    buf[i] = *c;
                    i++;
                }
                if (!*c)
                    state = STATE_FINISH;
                break;

            case STATE_LWSP:
                tmp = c;
                ++tmp;
                for (; *tmp ; ++tmp)
                {
                    if (*tmp == '\r')
                    {
                        c = tmp;
                        state = STATE_CRLF;
                        break;
                    }
                    if (*tmp != ' ' && *tmp != '\t')
                    {
                        len = tmp - c;
                        strncat(buf, c, len);
                        i += len;
                        c = tmp;
                        state = STATE_WRITE;
                        break;
                    }
                }
                if (!*tmp)
                    state = STATE_FINISH;
                break;

            case STATE_CRLF:
                for (; *c ; ++c)
                {
                    if (*c == ' ' || *c == '\t')
                    {
                        state = STATE_LWSP;
                        break;
                    }
                    if (*c != '\r' && *c != '\n')
                    {
                        state = STATE_WRITE;
                        break;
                    }
                }
                if (!*tmp)
                    state = STATE_FINISH;
                break;

            case STATE_FINISH:
                break;

            default:
                printf("!!!!!!!!!!! Character uncaught !!!!!!!!!!!!.\n");
                break;
        }
    }

    len = strlen(buf);
    unfold = calloc(len+1, sizeof(char));
    strncpy(unfold, buf, len);

    return unfold;
}

static void PrintKey(Item *item)
{
    lwd_request_header_t *key = (lwd_request_header_t *)item->value;
    printf("Key: %d\nValue: %s\n", key->key, key->value);
}

/* 
    HEADERS MUST point to the first character of the headers part of the HTTP request
*/
static void REQUEST_ParseHeaders(lwd_request_t *r, char *headers)
{
    if (*headers == '\r' || !headers) // there is no headers
    {
        return;
    }

    lwd_request_header_t *key;
    lwd_list_t *header_list;
    char *h, *p, *tmp;
    size_t hlen = 0;

    header_list = DLL_NewList();

    p = headers;
    p = strchr(p, '\n'); // Jump method line
    ++p;
    while (p)
    {
        if (*p == '\r') // End of headers
            break;
        
        h = REQUEST_IsolateHeader(p);
        // Use regex
        hlen = strlen(h);
        p += hlen+2; // +2 to jump CRLF and point to the first char of next line
        tmp = h;
        h = REQUEST_UnfoldHeader(h);
        free(tmp);
        key = REQUEST_ParseHeader(h);
        free(h);
        DLL_AddRight(header_list, (void *)key);
    }

    // DLL_ForEach(header_list, PrintKey); // Print header_list

    r->headers = header_list;
}

static lwd_request_t *REQUEST_Init(void)
{
    return (lwd_request_t *)calloc(1, sizeof(lwd_request_t));
}

lwd_request_t *REQUEST_ParseRequest(char *request)
{
    lwd_request_t *req = REQUEST_Init();

    if (!REQUEST_ParseStatusLine(req, request))
    {
        goto Error;
    }
    REQUEST_ParseHeaders(req, request);

    return req;

Error:
    REQUEST_Free(req);
    return NULL;
}

void REQUEST_Free(lwd_request_t *r)
{
    if (!r)
        return;

    REQUEST_FreeHeadersList(r);

    if (r->protocol)
        free(r->protocol);
    if (r->uri)
        free(r->uri);

    free(r);
}