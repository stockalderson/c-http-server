#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>

#include "http.h"
#include "mime/mime.h"
#include "../../../dll/dll.h"
#include "request/request.h"
#include "process/process.h"

lwd_http_t *HTTP_New(void)
{
    /* Declaration */

    lwd_http_t *http;
    
    /* Initialisation */

    http = (lwd_http_t *)calloc(1, sizeof(lwd_http_t));

    PROCESS_Init();
    http->response = RESPONSE_Init();

    return http;
}

lwd_http_t *HTTP_Free(lwd_http_t *http)
{
    if (http == NULL)
        return NULL;
    if (http->response)
    {
        RESPONSE_Free(http->response);
        http->response = NULL;
    }
    // Free Process
    PROCESS_Free();
    if (http->request)
    {
        REQUEST_Free(http->request);
        http->request = NULL;
    }
    free(http);

    return NULL;
}

bool HTTP_IsTLS(char *request)
{
    if (request[0] == SYN || request[0] == ACK) // SYN bit for TLS connection
    {
        return true;
    }
    return false;
}

// Apprendre à initier un Handshake TCP
bool HTTP_HandleTCPHandshake(char *request, int rsockfd)
{
    char buf[16];
    memset(buf, 0, sizeof(buf));

    size_t len = strlen(request);
    printf("request bytes = ");
    for (int i=0 ; i<len ; i++)
    {
        printf("%d.", request[i]);
    }

    return false;
}

/*
    Parse REQUEST into HTTP->request
    Return 1 on success or 0 on error
*/
bool HTTP_ParseRequest(lwd_http_t *http, char *request)
{
    if (!request || strlen(request) == 0)
    {
        printf("%s:%d : request is null or empty.\n", __FILE__, __LINE__);
        http_status = BAD_REQUEST;
        return false;
    }

    http->request = REQUEST_ParseRequest(request);
    if (http->request == NULL)
    {
        return false;
    }

    return true;
}

/* 
    Change the method with which we process headers.
*/
bool HTTP_ProcessRequest(lwd_http_t *http, const char *wwwroot)
{
    // Process headers

    if (!PROCESS_Resource((char *)wwwroot, http->request->uri))
    {
        printf("PROCESS_Resource failed.\n");
        return false;
    }

    return true;
}

bool HTTP_GenerateResponse(lwd_http_t *http)
{
    if (!http)
    {
        printf("%s:%d : Error : struct http is null.\n", __FILE__, __LINE__);
        return INTERNAL_SERVER_ERROR;
    }

    RESPONSE_Set(http->response, http->request->protocol);

    if (!RESPONSE_GenerateResponse(http->response))
    {
        return false;
    }

    return true;
}

bool HTTP_SendResponse(lwd_http_t *http, int writefd)
{
    if (writefd < 0)
    {
        printf("%s:%d : Error : write file descriptor is not valid.\n", __FILE__, __LINE__);
        http_status = INTERNAL_SERVER_ERROR;
        lwd_error = LWD_ERROR_BAD_FD;
        return false;
    }

    // Send Header
    if (write(writefd, http->response->response, strlen(http->response->response)) == -1)
    {
        printf("%s:%d : write() failed : %s\n", __FILE__, __LINE__, strerror(errno));
        http_status = INTERNAL_SERVER_ERROR;
        return false;
    }
    printf("Response sent.\n");

    // Send Body
    size_t n = 0;
    size_t total_bytes_sent = 0;
    size_t total_size = PROCESS_GetFileSize();
    size_t bytes_left = total_size;
    char *buf = PROCESS_GetResourceContent();

    while (total_bytes_sent < total_size)
    {
        if ((n = write(writefd, buf, bytes_left)) == -1)
        {
            printf("write failed : %s\n", strerror(errno));
            http_status = INTERNAL_SERVER_ERROR;
            return false;
        }
        bytes_left -= n;
        total_bytes_sent += n;
        buf += n;
    }
    printf("Resource sent.\n\n");

    return true;
}

void HTTP_ErrorPage(lwd_http_t *http, int rsockfd)
{
    if (rsockfd < 0)
    {
        printf("rsockfd doesn't exist.\n");
        return;
    }

    /* Declaration */

    int page_len;
    int err;
    char header[BUFSIZ];
    char error_html[BUFSIZ];
    char *status_phrase;

    /* Initialisation */

    status_phrase = str_status_code(http_status);
    if (!status_phrase)
    {
        printf("%s:%d : Error : str_status_code() failed.\n", __FILE__, __LINE__);
        return;
    }

    err = snprintf(error_html, sizeof(error_html),\
    "<html>\r\n"
    "   <head>\r\n"
    "       <meta charset=\"utf-8\">\r\n"
    "       <title>ERROR PAGE</title>\r\n"
    "   </head>\r\n"
    "   <body>\r\n"
    "       <div class=\"menu\">\r\n"
    "           <h1 style=\"text-align: center;\">LightWeigHTTPd</h1>\r\n"
    "           <br>\r\n"
    "           <hr width=\"1850\">\r\n"
    "           <br>\r\n"
    "           <h3 style=\"text-align: center;\">%d %s</h3>\r\n"
    "       </div>\r\n"
    "   </body>\r\n"
    "</html>\r\n\r\n", http_status, status_phrase);
    if (err < 0)
    {
        printf("%s:%d : snprintf() failed : %s\n", __FILE__, __LINE__, strerror(errno));
        status_phrase = str_status_code_free(status_phrase);
        return;
    }

    page_len = strlen(error_html);
    char mime_type[] = "text/html";

    err = snprintf(header, BUFSIZ, "HTTP/1.1 %d %s\r\n"
                                   "Content-Type: %s\r\n"
                                   "Content-Length: %d\r\n"
                                   "\r\n", http_status, status_phrase, mime_type, page_len);
    if (err < 0)
    {
        printf("%s:%d : snprintf() failed : %s\n", __FILE__, __LINE__, strerror(errno));
        status_phrase = str_status_code_free(status_phrase);
        return;
    }

    /* Action */

    printf("Sending Response...\n");
    if (write(rsockfd, header, strlen(header)) == -1)
    {
        printf("%s:%d : write() failed : %s\n", __FILE__, __LINE__, strerror(errno));
        status_phrase = str_status_code_free(status_phrase);
        return;
    }
    printf("Response sent\n");

    printf("Sending Body...\n");
    if (write(rsockfd, error_html, page_len) == -1)
    {
        printf("%s:%d : write() failed : %s\n", __FILE__, __LINE__, strerror(errno));
        status_phrase = str_status_code_free(status_phrase);
        return;
    }
    printf("Body sent.\n");

    /* Leave */

    printf("Error Page sent : \n%s", error_html);
    status_phrase = str_status_code_free(status_phrase);

    return; 
}