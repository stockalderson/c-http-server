#ifndef __LWD_MIME_H__
#define __LWD_MIME_H__

typedef enum {
    MIME_ERROR,  // 0
    MIME_AAC,    // 1
    MIME_ABW,    // 2
    MIME_ARC,    // 3
    MIME_AVIF,   // 4
    MIME_AVI,    // 5
    MIME_AZW,    // 6
    MIME_BIN,    // 7
    MIME_BMP,    // 8
    MIME_BZ,     // 9
    MIME_BZ2,    // 10
    MIME_CDA,    // 11
    MIME_CSH,    // 12
    MIME_CSS,    // 13
    MIME_CSV,    // 14
    MIME_DOC,    // 15
    MIME_DOCX,   // 16
    MIME_EOT,    // 17
    MIME_EPUB,   // 18
    MIME_GZ,     // 19
    MIME_GIF,    // 20
    MIME_HTML,   // 21
    MIME_ICO,    // 22
    MIME_ICS,    // 23
    MIME_JAR,    // 24
    MIME_JPEG,   // 25
    MIME_JPG,    // 26
    MIME_JS,     // 27
    MIME_JSON,   // 28
    MIME_JSONLD, // 29
    MIME_MID,    // 30
    MIME_MIDI,   // 31
    MIME_MJS,    // 32
    MIME_MP3,    // 33
    MIME_MP4,    // 34
    MIME_MPEG,   // 35
    MIME_MPKG,   // 36
    MIME_ODP,    // 37
    MIME_ODS,    // 38
    MIME_ODT,    // 39
    MIME_OGA,    // 40
    MIME_OGV,    // 41
    MIME_OGX,    // 42
    MIME_OPUS,   // 43
    MIME_OTF,    // 44
    MIME_PNG,    // 45
    MIME_PDF,    // 46
    MIME_PHP,    // 47
    MIME_PPT,    // 48
    MIME_PPTX,   // 49
    MIME_RAR,    // 50
    MIME_RTF,    // 51
    MIME_SH,     // 52
    MIME_SVG,    // 53
    MIME_SWF,    // 54
    MIME_TAR,    // 55
    MIME_TIFF,   // 56
    MIME_TS,     // 57
    MIME_TTF,    // 58
    MIME_TXT,    // 59
    MIME_VSD,    // 60
    MIME_WAV,    // 61
    MIME_WEBA,   // 62
    MIME_WEBM,   // 63
    MIME_WEBP,   // 64
    MIME_WOFF,   // 65
    MIME_WOFF2,  // 66
    MIME_XHTML,  // 67
    MIME_XLS,    // 68
    MIME_XLSX,   // 69
    MIME_XML,    // 70
    MIME_XUL,    // 71
    MIME_ZIP,    // 72
    MIME_7Z,     // 73
    MIME_ANY,
    MIME_DEFAULT,
    MIME_NOT_IMPLEMENTED,
} lwd_mime_code_e;

char *MIME_FindByCode(lwd_mime_code_e mime_code);
lwd_mime_code_e MIME_FindByExtension(char *ext);

#endif