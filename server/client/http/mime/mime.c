#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "mime.h"
#include "../../../../useful/useful.h"
#include "../../../../fsm/fsm.h"

typedef enum {
    STATE_ERROR,
    STATE_START,
    STATE_END,
    STATE_EXIT,
    STATE_A,
    STATE_AA,
    STATE_AAC,
    STATE_AB,
    STATE_ABW,
    STATE_AR,
    STATE_ARC,
    STATE_AV,
    STATE_AVI,
    STATE_AVIF,
    STATE_AZ,
    STATE_AZW,
    STATE_B,
    STATE_BI,
    STATE_BIN,
    STATE_BM,
    STATE_BMP,
    STATE_BZ,
    STATE_BZ2,
    STATE_C,
    STATE_CD,
    STATE_CDA,
    STATE_CS,
    STATE_CSH,
    STATE_CSS,
    STATE_CSV,
    STATE_D,
    STATE_DOC,
    STATE_DOCX,
    STATE_E,
    STATE_EO,
    STATE_EOT,
    STATE_EP,
    STATE_EPUB,
    STATE_G,
    STATE_GZ,
    STATE_GI,
    STATE_GIF,
    STATE_H,
    STATE_HTML,
    STATE_I,
    STATE_IC,
    STATE_ICO,
    STATE_ICS,
    STATE_J,
    STATE_JA,
    STATE_JAR,
    STATE_JP,
    STATE_JPG,
    STATE_JPEG,
    STATE_JS,
    STATE_JSON,
    STATE_JSONLD,
    STATE_M,
    STATE_MI,
    STATE_MID,
    STATE_MIDI,
    STATE_MJ,
    STATE_MJS,
    STATE_MP,
    STATE_MP3,
    STATE_MP4,
    STATE_MPE,
    STATE_MPEG,
    STATE_MPK,
    STATE_MPKG,
    STATE_O,
    STATE_OD,
    STATE_ODT,
    STATE_ODP,
    STATE_ODS,
    STATE_OG,
    STATE_OGA,
    STATE_OGV,
    STATE_OGX,
    STATE_OP,
    STATE_OPUS,
    STATE_OT,
    STATE_OTF,
    STATE_P,
    STATE_PN,
    STATE_PNG,
    STATE_PD,
    STATE_PDF,
    STATE_PH,
    STATE_PHP,
    STATE_PP,
    STATE_PPT,
    STATE_PPTX,
    STATE_R,
    STATE_RA,
    STATE_RAR,
    STATE_RT,
    STATE_RTF,
    STATE_S,
    STATE_SH,
    STATE_SV,
    STATE_SVG,
    STATE_SW,
    STATE_SWF,
    STATE_T,
    STATE_TA,
    STATE_TAR,
    STATE_TI,
    STATE_TIFF,
    STATE_TS,
    STATE_TT,
    STATE_TTF,
    STATE_TX,
    STATE_TXT,
    STATE_V,
    STATE_VSD,
    STATE_W,
    STATE_WA,
    STATE_WAV,
    STATE_WE,
    STATE_WEB,
    STATE_WEBA,
    STATE_WEBM,
    STATE_WEBP,
    STATE_WO,
    STATE_WOFF,
    STATE_WOFF2,
    STATE_X,
    STATE_XH,
    STATE_XHTML,
    STATE_XL,
    STATE_XLS,
    STATE_XLSX,
    STATE_XM,
    STATE_XML,
    STATE_XU,
    STATE_XUL,
    STATE_Z,
    STATE_ZIP,
    STATE_7,
    STATE_7Z,
    STATE_NOT_IMPLEMENTED,
} lwd_mime_state_e;

static lwd_mime_state_e MStart(int c);
static lwd_mime_state_e MStartA(int c);
static lwd_mime_state_e MStartB(int c);
static lwd_mime_state_e MStartC(int c);
static lwd_mime_state_e MStartCS(int c);
static lwd_mime_state_e MStartE(int c);
static lwd_mime_state_e MStartG(int c);
static lwd_mime_state_e MStartI(int c);
static lwd_mime_state_e MStartJ(int c);
static lwd_mime_state_e MStartM(int c);
static lwd_mime_state_e MStartMP(int c);
static lwd_mime_state_e MStartO(int c);
static lwd_mime_state_e MStartOD(int c);
static lwd_mime_state_e MStartOG(int c);
static lwd_mime_state_e MStartP(int c);
static lwd_mime_state_e MStartR(int c);
static lwd_mime_state_e MStartS(int c);
static lwd_mime_state_e MStartT(int c);
static lwd_mime_state_e MStartW(int c);
static lwd_mime_state_e MStartWE(int c);
static lwd_mime_state_e MStartWEB(int c);
static lwd_mime_state_e MStartX(int c);
static int IsMimeType(char *ext, const char *cmp);

char *MIME_FindByCode(lwd_mime_code_e mime_code)
{
    /* Declaration */

    char *mime_str;
    int len;

    /* Initialisation */

    mime_str = NULL;
    len = 0;

    switch (mime_code)
    {
        case MIME_ERROR:
            printf("ERROR : No mime type found\n");
            break;

        case MIME_AAC:
            len = strlen("audio/aac");
            mime_str = alloc_n_cpy("audio/aac", len);
            break;

        case MIME_ABW:
            len = strlen("application/x-abiword");
            mime_str = alloc_n_cpy("application/x-abiword", len);
            break;

        case MIME_ARC:
            len = strlen("application/x-freearc");
            mime_str = alloc_n_cpy("application/x-freearc", len);
            break;

        case MIME_AVIF:
            len = strlen("image/avif");
            mime_str = alloc_n_cpy("image/avif", len);
            break;

        case MIME_AVI:
            len = strlen("video/x-msvideo");
            mime_str = alloc_n_cpy("video/x-msvideo", len);
            break;

        case MIME_AZW:
            len = strlen("application/vnd.amazon.ebook");
            mime_str = alloc_n_cpy("application/vnd.amazon.ebook", len);
            break;

        case MIME_BIN:
            len = strlen("application/octet-stream");
            mime_str = alloc_n_cpy("application/octet-stream", len);
            break;

        case MIME_BMP:
            len = strlen("image/bmp");
            mime_str = alloc_n_cpy("image/bmp", len);
            break;

        case MIME_BZ:
            len = strlen("application/x-bzip");
            mime_str = alloc_n_cpy("application/x-bzip", len);
            break;

        case MIME_BZ2:
            len = strlen("application/x-bzip2");
            mime_str = alloc_n_cpy("application/x-bzip2", len);
            break;

        case MIME_CDA:
            len = strlen("application/x-cdf");
            mime_str = alloc_n_cpy("application/x-cdf", len);
            break;

        case MIME_CSH:
            len = strlen("application/x-csh");
            mime_str = alloc_n_cpy("application/x-csh", len);
            break;

        case MIME_CSS:
            len = strlen("text/css");
            mime_str = alloc_n_cpy("text/css", len);
            break;

        case MIME_CSV:
            len = strlen("text/csv");
            mime_str = alloc_n_cpy("text/csv", len);
            break;

        case MIME_DOC:
            len = strlen("application/msword");
            mime_str = alloc_n_cpy("application/msword", len);
            break;

        case MIME_DOCX:
            len = strlen("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
            mime_str = alloc_n_cpy("application/vnd.openxmlformats-officedocument.wordprocessingml.document", len);
            break;

        case MIME_EOT:
            len = strlen("application/vnd.ms-fontobject");
            mime_str = alloc_n_cpy("application/vnd.ms-fontobject", len);
            break;

        case MIME_EPUB:
            len = strlen("application/epub+zip");
            mime_str = alloc_n_cpy("application/epub+zip", len);
            break;

        case MIME_GZ:
            len = strlen("application/gzip");
            mime_str = alloc_n_cpy("application/gzip", len);
            break;

        case MIME_GIF:
            len = strlen("image/gif");
            mime_str = alloc_n_cpy("image/gif", len);
            break;

        case MIME_HTML:
            len = strlen("text/html");
            mime_str = alloc_n_cpy("text/html", len);
            break;

        case MIME_ICO:
            len = strlen("image/vnd.microsoft.icon");
            mime_str = alloc_n_cpy("image/vnd.microsoft.icon", len);
            break;

        case MIME_ICS:
            len = strlen("text/calendar");
            mime_str = alloc_n_cpy("text/calendar", len);
            break;

        case MIME_JAR:
            len = strlen("application/java-archive");
            mime_str = alloc_n_cpy("application/java-archive", len);
            break;

        case MIME_JPEG:
            len = strlen("image/jpeg");
            mime_str = alloc_n_cpy("image/jpeg", len);
            break;

        case MIME_JPG:
            len = strlen("image/jpeg");
            mime_str = alloc_n_cpy("image/jpeg", len);
            break;

        case MIME_JS:
            len = strlen("text/javascript");
            mime_str = alloc_n_cpy("text/javascript", len);
            break;

        case MIME_JSON:
            len = strlen("application/json");
            mime_str = alloc_n_cpy("application/json", len);
            break;

        case MIME_JSONLD:
            len = strlen("application/ld+json");
            mime_str = alloc_n_cpy("application/ld+json", len);
            break;

        case MIME_MID:
            len = strlen("audio/midi");
            mime_str = alloc_n_cpy("audio/midi", len);
            break;

        case MIME_MIDI:
            len = strlen("audio/x-midi");
            mime_str = alloc_n_cpy("audio/x-midi", len);
            break;

        case MIME_MJS:
            len = strlen("text/javascript");
            mime_str = alloc_n_cpy("text/javascript", len);
            break;

        case MIME_MP3:
            len = strlen("audio/mpeg");
            mime_str = alloc_n_cpy("audio/mpeg", len);
            break;

        case MIME_MP4:
            len = strlen("video/mp4");
            mime_str = alloc_n_cpy("video/mp4", len);
            break;

        case MIME_MPEG:
            len = strlen("video/mpeg");
            mime_str = alloc_n_cpy("video/mpeg", len);
            break;

        case MIME_MPKG:
            len = strlen("application/vnd.apple.installer+xml");
            mime_str = alloc_n_cpy("application/vnd.apple.installer+xml", len);
            break;

        case MIME_ODP:
            len = strlen("application/vnd.oasis.opendocument.presentation");
            mime_str = alloc_n_cpy("application/vnd.oasis.opendocument.presentation", len);
            break;

        case MIME_ODS:
            len = strlen("application/vnd.oasis.opendocument.spreadsheet");
            mime_str = alloc_n_cpy("application/vnd.oasis.opendocument.spreadsheet", len);
            break;

        case MIME_ODT:
            len = strlen("application/vnd.oasis.opendocument.text");
            mime_str = alloc_n_cpy("application/vnd.oasis.opendocument.text", len);
            break;

        case MIME_OGA:
            len = strlen("audio/ogg");
            mime_str = alloc_n_cpy("audio/ogg", len);
            break;

        case MIME_OGV:
            len = strlen("video/ogg");
            mime_str = alloc_n_cpy("video/ogg", len);
            break;

        case MIME_OGX:
            len = strlen("application/ogg");
            mime_str = alloc_n_cpy("application/ogg", len);
            break;

        case MIME_OPUS:
            len = strlen("audio/opus");
            mime_str = alloc_n_cpy("audio/opus", len);
            break;

        case MIME_OTF:
            len = strlen("font/otf");
            mime_str = alloc_n_cpy("font/otf", len);
            break;

        case MIME_PNG:
            len = strlen("image/png");
            mime_str = alloc_n_cpy("image/png", len);
            break;

        case MIME_PDF:
            len = strlen("application/pdf");
            mime_str = alloc_n_cpy("application/pdf", len);
            break;

        case MIME_PHP:
            len = strlen("application/x-httpd-php");
            mime_str = alloc_n_cpy("application/x-httpd-php", len);
            break;

        case MIME_PPT:
            len = strlen("application/vnd.ms-powerpoint");
            mime_str = alloc_n_cpy("application/vnd.ms-powerpoint", len);
            break;

        case MIME_PPTX:
            len = strlen("application/vnd.openxmlformats-officedocument.presentationml.presentation");
            mime_str = alloc_n_cpy("application/vnd.openxmlformats-officedocument.presentationml.presentation", len);
            break;

        case MIME_RAR:
            len = strlen("application/vnd.rar");
            mime_str = alloc_n_cpy("application/vnd.rar", len);
            break;

        case MIME_RTF:
            len = strlen("application/rtf");
            mime_str = alloc_n_cpy("application/rtf", len);
            break;

        case MIME_SH:
            len = strlen("application/x-sh");
            mime_str = alloc_n_cpy("application/x-sh", len);
            break;

        case MIME_SVG:
            len = strlen("image/svg+xml");
            mime_str = alloc_n_cpy("image/svg+xml", len);
            break;

        case MIME_SWF:
            len = strlen("application/x-shockwave-flash");
            mime_str = alloc_n_cpy("application/x-shockwave-flash", len);
            break;

        case MIME_TAR:
            len = strlen("application/x-tar");
            mime_str = alloc_n_cpy("application/x-tar", len);
            break;

        case MIME_TIFF:
            len = strlen("image/tiff");
            mime_str = alloc_n_cpy("image/tiff", len);
            break;

        case MIME_TS:
            len = strlen("video/mp2t");
            mime_str = alloc_n_cpy("video/mp2t", len);
            break;

        case MIME_TTF:
            len = strlen("font/ttf");
            mime_str = alloc_n_cpy("font/ttf", len);
            break;

        case MIME_TXT:
            len = strlen("text/plain");
            mime_str = alloc_n_cpy("text/plain", len);
            break;

        case MIME_VSD:
            len = strlen("application/vnd.visio");
            mime_str = alloc_n_cpy("application/vnd.visio", len);
            break;

        case MIME_WAV:
            len = strlen("audio/wav");
            mime_str = alloc_n_cpy("audio/wav", len);
            break;

        case MIME_WEBA:
            len = strlen("audio/webm");
            mime_str = alloc_n_cpy("audio/webm", len);
            break;

        case MIME_WEBM:
            len = strlen("video/webm");
            mime_str = alloc_n_cpy("video/webm", len);
            break;

        case MIME_WEBP:
            len = strlen("image/webp");
            mime_str = alloc_n_cpy("image/webp", len);
            break;

        case MIME_WOFF:
            len = strlen("font/woff");
            mime_str = alloc_n_cpy("font/woff", len);
            break;

        case MIME_WOFF2:
            len = strlen("font/woff2");
            mime_str = alloc_n_cpy("font/woff2", len);
            break;

        case MIME_XHTML:
            len = strlen("application/xhtml+xml");
            mime_str = alloc_n_cpy("application/xhtml+xml", len);
            break;

        case MIME_XLS:
            len = strlen("application/vnd.ms-excel");
            mime_str = alloc_n_cpy("application/vnd.ms-excel", len);
            break;

        case MIME_XLSX:
            len = strlen("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            mime_str = alloc_n_cpy("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", len);
            break;

        case MIME_XML:
            len = strlen("application/xml");
            mime_str = alloc_n_cpy("application/xml", len);
            break;

        case MIME_XUL:
            len = strlen("application/vnd.mozilla.xul+xml");
            mime_str = alloc_n_cpy("application/vnd.mozilla.xul+xml", len);
            break;

        case MIME_ZIP:
            len = strlen("application/zip");
            mime_str = alloc_n_cpy("application/zip", len);
            break;

        case MIME_7Z:
            len = strlen("application/x-7z-compressed");
            mime_str = alloc_n_cpy("application/x-7z-compressed", len);
            break;

        case MIME_DEFAULT:
            len = strlen("application/octet-stream");
            mime_str = alloc_n_cpy("application/octet-stream", len);
            break;

        default:
            printf("No mime type found : mime_str = %p\n", mime_str);
            return NULL;
            break;
    }

    return mime_str;
}

lwd_mime_code_e MIME_FindByExtension(char *ext)
{
    if (ext == NULL)
    {
        printf("extension is null.\n");
        return MIME_ERROR;
    }

    /* Declaration */

    lwd_fsm_t fsm;
    lwd_mime_code_e mime_code;
    char *c = ext;

    /* Initialisation */

    memset(&fsm, 0, sizeof(fsm));

    fsm.state = STATE_START;
    mime_code = MIME_ERROR;

    while (fsm.state != STATE_EXIT)
    {
        fsm.loop_count++;
        switch (fsm.state)
        {
            case STATE_START:
                fsm.state = MStart(*c);
                ++c;
                break;

            case STATE_A:
                fsm.state = MStartA(*c);
                ++c;
                switch (fsm.state)
                {
                    case STATE_AA:
                        if (IsMimeType(ext, "aac"))
                            mime_code = MIME_AAC;
                        fsm.state = STATE_EXIT;
                        break;

                    case STATE_AB:
                        if (IsMimeType(ext, "abw"))
                            mime_code = MIME_ABW;
                        fsm.state = STATE_EXIT;
                        break;

                    case STATE_AR:
                        if (IsMimeType(ext, "arc"))
                            mime_code = MIME_ARC;
                        fsm.state = STATE_EXIT;
                        break;

                    case STATE_AV:
                        if (IsMimeType(ext, "avi"))
                            mime_code = MIME_AVI;
                        else if (IsMimeType(ext, "avif"))
                            mime_code = MIME_AVIF;
                        fsm.state = STATE_EXIT;
                        break;
                    
                    case STATE_AZ:
                        if (IsMimeType(ext, "azw"))
                            mime_code = MIME_AZW;
                        fsm.state = STATE_EXIT;
                        break;
                }
                break;

            case STATE_B:
                fsm.state = MStartB(*c);
                ++c;
                switch (fsm.state)
                {
                    case STATE_BI:
                        if (IsMimeType(ext, "bin"))
                            mime_code = MIME_BIN;
                        fsm.state = STATE_EXIT;
                        break;
                    case STATE_BM:
                        if (IsMimeType(ext, "bmp"))
                            mime_code = MIME_BMP;
                        fsm.state = STATE_EXIT;
                        break;
                    case STATE_BZ:
                        if (IsMimeType(ext, "bz"))
                            mime_code = MIME_BZ;
                        else if (IsMimeType(ext, "bz2"))
                            mime_code = MIME_BZ2;
                        fsm.state = STATE_EXIT;
                        break;
                }
                break;

            case STATE_C:
                fsm.state = MStartC(*c);
                ++c;
                switch (fsm.state)
                {
                    case STATE_CD:
                        if (IsMimeType(ext, "cda"))
                            mime_code = MIME_CDA;
                        fsm.state = STATE_EXIT;
                        break;

                    case STATE_CS:
                        fsm.state = MStartCS(*c);
                        ++c;
                        switch (fsm.state)
                        {
                            case STATE_CSH:
                                if (IsMimeType(ext, "csh"))
                                    mime_code = MIME_CSH;
                                fsm.state = STATE_EXIT;
                                break;

                            case STATE_CSS:
                                if (IsMimeType(ext, "css"))
                                    mime_code = MIME_CSS;
                                fsm.state = STATE_EXIT;
                                break;

                            case STATE_CSV:
                                if (IsMimeType(ext, "csv"))
                                    mime_code = MIME_CSV;
                                fsm.state = STATE_EXIT;
                                break;
                        }
                        break;
                }
                break;

            case STATE_D:
                if (IsMimeType(ext, "doc"))
                    mime_code = MIME_DOC;
                else if (IsMimeType(ext, "docx"))
                    mime_code = MIME_DOCX;
                fsm.state = STATE_EXIT;
                break;

            case STATE_E:
                fsm.state = MStartE(*c);
                ++c;
                switch (fsm.state)
                {
                    case STATE_EO:
                        if (IsMimeType(ext, "eot"))
                            mime_code = MIME_EOT;
                        fsm.state = STATE_EXIT;
                        break;
                    case STATE_EP:
                        if (IsMimeType(ext, "epub"))
                            mime_code = MIME_EPUB;
                        fsm.state = STATE_EXIT;
                        break;
                }
                break;

            case STATE_G:
                fsm.state = MStartG(*c);
                ++c;
                switch (fsm.state)
                {
                    case STATE_GI:
                        if (IsMimeType(ext, "gif"))
                            mime_code = MIME_GIF;
                        fsm.state = STATE_EXIT;
                        break;
                    case STATE_GZ:
                        if (IsMimeType(ext, "gz"))
                            mime_code = MIME_GZ;
                        fsm.state = STATE_EXIT;
                        break;
                }
                break;

            case STATE_H:
                if (IsMimeType(ext, "html"))
                    mime_code = MIME_HTML;
                fsm.state = STATE_EXIT;
                break;

            case STATE_I:
                fsm.state = MStartI(*c);
                ++c;
                switch (fsm.state)
                {
                    case STATE_IC:
                        if (IsMimeType(ext, "ico"))
                            mime_code = MIME_ICO;
                        else if (IsMimeType(ext, "ics"))
                            mime_code = MIME_ICS;
                        fsm.state = STATE_EXIT;
                        break;
                }
                break;

            case STATE_J:
                fsm.state = MStartJ(*c);
                ++c;
                switch (fsm.state)
                {
                    case STATE_JA:
                        if (IsMimeType(ext, "jar"))
                            mime_code = MIME_JAR;
                        fsm.state = STATE_EXIT;
                        break;
                    case STATE_JP:
                        if (IsMimeType(ext, "jpg"))
                            mime_code = MIME_JPG;
                        else if (IsMimeType(ext, "jpeg"))
                            mime_code = MIME_JPEG;
                        fsm.state = STATE_EXIT;
                        break;
                    case STATE_JS:
                        if (IsMimeType(ext, "js"))
                            mime_code = MIME_JS;
                        else if (IsMimeType(ext, "json"))
                            mime_code = MIME_JSON;
                        else if (IsMimeType(ext, "jsonld"))
                            mime_code = MIME_JSONLD;
                        fsm.state = STATE_EXIT;
                        break;
                }
                break;

            case STATE_M:
                fsm.state = MStartM(*c);
                ++c;
                switch (fsm.state)
                {
                    case STATE_MI:
                        if (IsMimeType(ext, "mid"))
                            mime_code = MIME_MID;
                        else if (IsMimeType(ext, "midi"))
                            mime_code = MIME_MIDI;
                    fsm.state = STATE_EXIT;
                    break;

                    case STATE_MJ:
                        if (IsMimeType(ext, "mjs"))
                            mime_code = MIME_MJS;
                        fsm.state = STATE_EXIT;
                        break;

                    case STATE_MP:
                        fsm.state = MStartMP(*c);
                        ++c;
                        switch (fsm.state)
                        {
                            case STATE_MP3:
                                if (IsMimeType(ext, "mp3"))
                                    mime_code = MIME_MP3;
                                fsm.state = STATE_EXIT;
                                break;

                            case STATE_MP4:
                                if (IsMimeType(ext, "mp4"))
                                    mime_code = MIME_MP4;
                                fsm.state = STATE_EXIT;
                                break;

                            case STATE_MPE:
                                if (IsMimeType(ext, "mpeg"))
                                    mime_code = MIME_MPEG;
                                fsm.state = STATE_EXIT;
                                break;

                            case STATE_MPK:
                                if (IsMimeType(ext, "mpkg"))
                                    mime_code = MIME_MPKG;
                                fsm.state = STATE_EXIT;
                                break;
                        }
                        break;
                }
                break;

            case STATE_O:
                fsm.state = MStartO(*c);
                ++c;
                switch (fsm.state)
                {
                    case STATE_OD:
                        fsm.state = MStartOD(*c);
                        ++c;
                        switch (fsm.state)
                        {
                            case STATE_ODP:
                                if (IsMimeType(ext, "odp"))
                                    mime_code = MIME_ODP;
                                fsm.state = STATE_EXIT;
                                break;

                            case STATE_ODS:
                                if (IsMimeType(ext, "ods"))
                                    mime_code = MIME_ODS;
                                fsm.state = STATE_EXIT;
                                break;

                            case STATE_ODT:
                                if (IsMimeType(ext, "odt"))
                                    mime_code = MIME_ODT;
                                fsm.state = MIME_ODT;
                                break;
                        }
                        break;

                    case STATE_OG:
                        fsm.state = MStartOG(*c);
                        ++c;
                        switch (fsm.state)
                        {
                            case STATE_OGA:
                                if (IsMimeType(ext, "oga"))
                                    mime_code = MIME_OGA;
                                fsm.state = STATE_EXIT;
                                break;

                            case STATE_OGV:
                                if (IsMimeType(ext, "ogv"))
                                    mime_code = MIME_OGV;
                                fsm.state = STATE_EXIT;
                                break;

                            case STATE_OGX:
                                if (IsMimeType(ext, "ogx"))
                                    mime_code = MIME_OGX;
                                fsm.state = STATE_EXIT;
                                break;
                        }
                        break;

                    case STATE_OP:
                        if (IsMimeType(ext, "opus"))
                            mime_code = MIME_OPUS;
                        fsm.state = STATE_EXIT;
                        break;

                    case STATE_OT:
                        if (IsMimeType(ext, "otf"))
                            mime_code = MIME_OTF;
                        fsm.state = STATE_EXIT;
                        break;
                }
                break;

            case STATE_P:
                fsm.state = MStartP(*c);
                ++c;
                switch (fsm.state)
                {
                    case STATE_PN:
                        if (IsMimeType(ext, "png"))
                            mime_code = MIME_PNG;
                        fsm.state = STATE_EXIT;
                        break;

                    case STATE_PD:
                        if (IsMimeType(ext, "pdf"))
                            mime_code = MIME_PDF;
                        fsm.state = STATE_EXIT;
                        break;

                    case STATE_PH:
                        if (IsMimeType(ext, "php"))
                            mime_code = MIME_PHP;
                        fsm.state = STATE_EXIT;
                        break;

                    case STATE_PP:
                        if (IsMimeType(ext, "ppt"))
                            mime_code = MIME_PPT;
                        else if (IsMimeType(ext, "pptx"))
                            mime_code = MIME_PPTX;
                        fsm.state = STATE_EXIT;
                        break;
                }
                break;

            case STATE_R:
                fsm.state = MStartR(*c);
                ++c;
                switch (fsm.state)
                {
                    case STATE_RA:
                        if (IsMimeType(ext, "rar"))
                            mime_code = MIME_RAR;
                        fsm.state = STATE_EXIT;
                        break;

                    case STATE_RT:
                        if (IsMimeType(ext, "rtf"))
                            mime_code = MIME_RTF;
                        fsm.state = STATE_EXIT;
                        break;
                }
                break;

            case STATE_S:
                fsm.state = MStartS(*c);
                ++c;
                switch (fsm.state)
                {
                    case STATE_SH:
                        if (IsMimeType(ext, "sh"))
                            mime_code = MIME_SH;
                        fsm.state = STATE_EXIT;
                        break;
                    
                    case STATE_SV:
                        if (IsMimeType(ext, "svg"))
                            mime_code = MIME_SVG;
                        fsm.state = STATE_EXIT;
                        break;

                    case STATE_SW:
                        if (IsMimeType(ext, "swf"))
                            mime_code = MIME_SWF;
                        fsm.state = STATE_EXIT;
                        break;
                }
                break;

            case STATE_T:
                fsm.state = MStartT(*c);
                ++c;
                switch (fsm.state)
                {
                    case STATE_TA:
                        if (IsMimeType(ext, "tar"))
                            mime_code = MIME_TAR;
                        fsm.state = STATE_EXIT;
                        break;

                    case STATE_TI:
                        if (IsMimeType(ext, "tif") || IsMimeType(ext, "tiff"))
                            mime_code = MIME_TIFF;
                        fsm.state = STATE_EXIT;
                        break;

                    case STATE_TS:
                        if (IsMimeType(ext, "ts"))
                            mime_code = MIME_TS;
                        fsm.state = STATE_EXIT;
                        break;

                    case STATE_TT:
                        if (IsMimeType(ext, "ttf"))
                            mime_code = MIME_TTF;
                        fsm.state = STATE_EXIT;
                        break;

                    case STATE_TX:
                        if (IsMimeType(ext, "txt"))
                            mime_code = MIME_TXT;
                        fsm.state = STATE_EXIT;
                        break;
                }
                break;

            case STATE_V:
                if (IsMimeType(ext, "vsd"))
                    mime_code = MIME_VSD;
                fsm.state = STATE_EXIT;
                break;

            case STATE_W:
                fsm.state = MStartW(*c);
                ++c;
                switch (fsm.state)
                {
                    case STATE_WA:
                        if (IsMimeType(ext, "wav"))
                            mime_code = MIME_WAV;
                        fsm.state = STATE_EXIT;
                        break;

                    case STATE_WE:
                        fsm.state = MStartWE(*c);
                        ++c;
                        switch (fsm.state)
                        {
                            case STATE_WEB:
                                fsm.state = MStartWEB(*c);
                                ++c;
                                switch (fsm.state)
                                {
                                    case STATE_WEBA:
                                        if (IsMimeType(ext, "weba"))
                                            mime_code = MIME_WEBA;
                                        fsm.state = STATE_EXIT;
                                        break;

                                    case STATE_WEBM:
                                        if (IsMimeType(ext, "webm"))
                                            mime_code = MIME_WEBM;
                                        fsm.state = STATE_EXIT;
                                        break;

                                    case STATE_WEBP:
                                        if (IsMimeType(ext, "webp"))
                                            mime_code = MIME_WEBP;
                                        fsm.state = STATE_EXIT;
                                        break;
                                }
                        }
                        break;

                    case STATE_WO:
                        if (IsMimeType(ext, "woff"))
                            mime_code = MIME_WOFF;
                        else if (IsMimeType(ext, "woff2"))
                            mime_code = MIME_WOFF2;
                        fsm.state = STATE_EXIT;
                        break;
                }
                break;

            case STATE_X:
                fsm.state = MStartX(*c);
                ++c;
                switch (fsm.state)
                {
                    case STATE_XH:
                        if (IsMimeType(ext, "xhtml"))
                            mime_code = MIME_XHTML;
                        fsm.state = STATE_EXIT;
                        break;

                    case STATE_XL:
                        if (IsMimeType(ext, "xls"))
                            mime_code = MIME_XLS;
                        else if (IsMimeType(ext, "xlsx"))
                            mime_code = MIME_XLSX;
                        fsm.state = STATE_EXIT;
                        break;

                    case STATE_XM:
                        if (IsMimeType(ext, "xml"))
                            mime_code = MIME_XML;
                        fsm.state = STATE_EXIT;
                        break;

                    case STATE_XU:
                        if (IsMimeType(ext, "xul"))
                            mime_code = MIME_XUL;
                        fsm.state = STATE_EXIT;
                        break;
                }

            case STATE_Z:
                if (IsMimeType(ext, "zip"))
                    mime_code = MIME_ZIP;
                fsm.state = STATE_EXIT;
                break;

            case STATE_7:
                if (IsMimeType(ext, "7z"))
                    mime_code = MIME_7Z;
                fsm.state = STATE_EXIT;
                break;

            case STATE_END:
                fsm.state = STATE_EXIT;
                break;

            case STATE_NOT_IMPLEMENTED:
                mime_code = MIME_NOT_IMPLEMENTED;
                fsm.state = STATE_EXIT;
                break;

            default:
                printf("MIME Code not found for extension: %s.\n", ext);
                mime_code = MIME_DEFAULT;
                fsm.state = STATE_EXIT;
                break;
        }
    }

    return mime_code;
}

static int IsMimeType(char *ext, const char *cmp)
{
    if (ext == NULL)
    {
        printf("extension is null.\n");
        return 0;
    }
    if (cmp == NULL)
    {
        printf("cmp is null.\n");
        return 0;
    }
    if (strcmp(ext, cmp) == 0)
    {
        return 1;
    }

    return 0;
}

static lwd_mime_state_e MStartX(int c)
{
    switch (c)
    {
        case 'h':
            return STATE_XH;
        case 'l':
            return STATE_XL;
        case 'm':
            return STATE_XM;
        case 'u':
            return STATE_XU;
        default:
            return STATE_ERROR;
    }
}

static lwd_mime_state_e MStartWEB(int c)
{
    switch (c)
    {
        case 'a':
            return STATE_WEBA;
        case 'm':
            return STATE_WEBM;
        case 'p':
            return STATE_WEBP;
        default:
            return STATE_EXIT;
    }
}

static lwd_mime_state_e MStartWE(int c)
{
    switch (c)
    {
        case 'b':
            return STATE_WEB;
        default:
            return STATE_ERROR;
    }
}

static lwd_mime_state_e MStartW(int c)
{
    switch (c)
    {
        case 'a':
            return STATE_WA;
        case 'e':
            return STATE_WE;
        case 'o':
            return STATE_WO;
        default:
            return STATE_ERROR;
    }
}

static lwd_mime_state_e MStartT(int c)
{
    switch (c)
    {
        case 'a':
            return STATE_TA;
        case 'i':
            return STATE_TI;
        case 's':
            return STATE_TS;
        case 't':
            return STATE_TT;
        case 'x':
            return STATE_TX;
        default:
            return STATE_ERROR;
    }
}

static lwd_mime_state_e MStartS(int c)
{
    switch (c)
    {
        case 'H':
            return STATE_SH;
        case 'V':
            return STATE_SV;
        case 'W':
            return STATE_SW;
        default:
            return STATE_ERROR;
    }
}

static lwd_mime_state_e MStartR(int c)
{
    switch (c)
    {
        case 'A':
            return STATE_RA;
        case 'T':
            return STATE_RT;
        default:
            return STATE_ERROR;
    }
}

static lwd_mime_state_e MStartP(int c)
{
    switch (c)
    {
        case 'n':
            return STATE_PN;
        case 'd':
            return STATE_PD;
        case 'h':
            return STATE_PH;
        case 'p':
            return STATE_PP;
        default:
            return STATE_ERROR;
    }
}

static lwd_mime_state_e MStartOG(int c)
{
    switch (c)
    {
        case 'a':
            return STATE_OGA;
        case 'v':
            return STATE_OGV;
        case 'x':
            return STATE_OGX;
        default:
            return STATE_ERROR;
    }
}

static lwd_mime_state_e MStartOD(int c)
{
    switch (c)
    {
        case 'p':
            return STATE_ODP;
        case 's':
            return STATE_ODS;
        case 't':
            return STATE_ODT;
        default:
            return STATE_ERROR;
    }
}

static lwd_mime_state_e MStartO(int c)
{
    switch (c)
    {
        case 'd':
            return STATE_OD;
        case 'g':
            return STATE_OG;
        case 'p':
            return STATE_OP;
        case 't':
            return STATE_OT;
        default:
            return STATE_ERROR;
    }
}

static lwd_mime_state_e MStartMP(int c)
{
    switch (c)
    {
        case '3':
            return STATE_MP3;
        case '4':
            return STATE_MP4;
        case 'e':
            return STATE_MPE;
        case 'k':
            return STATE_MPK;
        default:
            return STATE_ERROR;
    }
}

static lwd_mime_state_e MStartM(int c)
{
    switch (c)
    {
        case 'i':
            return STATE_MI;
        case 'j':
            return STATE_MJ;
        case 'p':
            return STATE_MP;
        default:
            return STATE_ERROR;
    }
}

static lwd_mime_state_e MStartJ(int c)
{
    switch (c)
    {
        case 'a':
            return STATE_JA;
        case 'p':
            return STATE_JP;
        case 's':
            return STATE_JS;
        default:
            return STATE_ERROR;
    }
}

static lwd_mime_state_e MStartI(int c)
{
    switch (c)
    {
        case 'c':
            return STATE_IC;
        default:
            return STATE_ERROR;
    }
}

static lwd_mime_state_e MStartG(int c)
{
    switch (c)
    {
        case 'i':
            return STATE_GI;
        case 'z':
            return STATE_GZ;
        default:
            return STATE_ERROR;
    }
}

static lwd_mime_state_e MStartE(int c)
{
    switch (c)
    {
        case 'o':
            return STATE_EO;
        case 'p':
            return STATE_EP;
        default:
            return MIME_ERROR;
    }
}

static lwd_mime_state_e MStartCS(int c)
{
    switch (c)
    {
        case 'h':
            return STATE_CSH;
        case 's':
            return STATE_CSS;
        case 'v':
            return STATE_CSV;
        default:
            return STATE_ERROR;
    }
}

static lwd_mime_state_e MStartC(int c)
{
    switch (c)
    {
        case 'd':
            return STATE_CD;
        case 's':
            return STATE_CS;
        default:
            return STATE_ERROR;
    }
}

static lwd_mime_state_e MStartB(int c)
{
    switch (c)
    {
        case 'i':
            return STATE_BI;
        case 'm':
            return STATE_BM;
        case 'z':
            return STATE_BZ;
        default:
            return STATE_ERROR;
    }
}

static lwd_mime_state_e MStartA(int c)
{
    switch (c)
    {
        case 'a':
            return STATE_AA;
        case 'b':
            return STATE_AB;
        case 'r':
            return STATE_AR;
        case 'v':
            return STATE_AV;
        case 'z':
            return STATE_AZ;
        default:
            return STATE_ERROR;
    }
}

static lwd_mime_state_e MStart(int c)
{
    switch (c)
    {
        case 'a':
            return STATE_A;
        case 'b':
            return STATE_B;
        case 'c':
            return STATE_C;
        case 'd':
            return STATE_D;
        case 'e':
            return STATE_E;
        case 'g':
            return STATE_G;
        case 'h':
            return STATE_H;
        case 'i':
            return STATE_I;
        case 'j':
            return STATE_J;
        case 'm':
            return STATE_M;
        case 'o':
            return STATE_O;
        case 'p':
            return STATE_P;
        case 'r':
            return STATE_R;
        case 's':
            return STATE_S;
        case 't':
            return STATE_T;
        case 'v':
            return STATE_V;
        case 'w':
            return STATE_W;
        case 'x':
            return STATE_X;
        case 'z':
            return STATE_Z;
        case '7':
            return STATE_7;
        default:
            return STATE_ERROR;
    }
}