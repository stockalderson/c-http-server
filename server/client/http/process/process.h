#ifndef PROCESS_H
#define PROCESS_H

#include <sys/stat.h>
#include <stdbool.h>

typedef struct {
    char *resource_path;
    char *resource_content;
    struct stat resource_info;
    char *mime_str;
} lwd_process_t;

bool PROCESS_Init(void);
void PROCESS_Free(void);

bool PROCESS_Resource(char *wwwroot, char *uri);

char *PROCESS_GetResourceContent(void);
size_t PROCESS_GetFileSize(void);
char *PROCESS_GetMime(void);

#endif