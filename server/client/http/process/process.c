#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

#include "../../../../error/error.h"
#include "../mime/mime.h"
#include "../response/response.h"
#include "process.h"

lwd_process_t *process = NULL;

void PROCESS_RequestHeaders(lwd_list_t *headers)
{
    ;
}

static char *get_resource_extension(void)
{
    char *p = process->resource_path;
    char *ext;
    int len;

    if (strchr(p, '/'))
    {
        p = strchr(p, 0);
        while (*p != '/')
            --p;
    }
    p = strchr(p, '.');
    ++p;
    len = strlen(p);
    ext = (char *)calloc(len+1, sizeof(char));
    strcpy(ext, p);
    ext[len] = 0;

    return ext;
}

static void set_path_to_resource(char *wwwroot, char *uri)
{
    char *path = NULL;
    size_t len;

    len = strlen(wwwroot) + strlen(uri);
    path = calloc(len+1, sizeof(char));
    strcpy(path, wwwroot);
    strcat(path, uri);
    path[len] = 0;

    process->resource_path = path;
}

/* 
    set_resource_info MUST be called before
*/
static bool set_resource_content(void)
{
    FILE *fd = NULL;
    char *content = NULL;

    if (process->resource_info.st_size <= 0)
    {
        printf("Error : resource has not content to read (Maybe call set_resource_info() before).\n");
        http_status = INTERNAL_SERVER_ERROR;
        return false;
    }

    fd = fopen(process->resource_path, "r");
    if (!fd)
    {
        printf("Resource location Error : Path = %s\n", process->resource_path);
        if (errno == ENOENT) // No such file
            http_status = NOT_FOUND;
        return false;
    }
    content = (char *)calloc(process->resource_info.st_size+1, sizeof(char));
    fread(content, process->resource_info.st_size, 1, fd);
    if (feof(fd) || ferror(fd))
    {
        http_status = INTERNAL_SERVER_ERROR;
        printf("Error could not read file.\n");
        free(content);
        fclose(fd);
        return false;
    }
    fclose(fd);
    content[process->resource_info.st_size] = 0;

    process->resource_content = content;

    return true;
}

static bool set_resource_info(void)
{
    if (stat(process->resource_path, &process->resource_info) == -1)
    {
        if (errno == ENOENT)
            http_status = NOT_FOUND;
        else
            http_status = INTERNAL_SERVER_ERROR;
        printf("Error resource location : Path = %s\n", process->resource_path);
        return false;
    }

    return true;
}

static bool set_mime_code(void)
{
    char *mime_type, *ext;
    lwd_mime_code_e mime_code;

    /* Initialisation */

    ext = get_resource_extension();
    mime_code = MIME_FindByExtension(ext);
    free(ext);
    if (mime_code == MIME_ERROR)
    {
        http_status = NOT_IMPLEMENTED;
        printf("mime code not found\n");
        return false;
    }
    mime_type = MIME_FindByCode(mime_code);
    if (!mime_type)
    {
        printf("%s:%d : MIME_FindByCode() failed : mime code : %d\n", __FILE__, __LINE__, mime_code);
        http_status = INTERNAL_SERVER_ERROR;
        return false;
    }

    process->mime_str = mime_type;

    return true;
}

bool PROCESS_Init(void)
{
    if (process)
        return true;

    process = (lwd_process_t *)calloc(1, sizeof(lwd_process_t));

    return true;
}

void PROCESS_Free(void)
{
    if (!process)
        return;

    if (process->resource_path)
        free(process->resource_path);
    if (process->resource_content)
        free(process->resource_content);
    if (process->mime_str)
        free(process->mime_str);
    free(process);
    process = NULL;
}

bool PROCESS_Resource(char *wwwroot, char *uri)
{
    set_path_to_resource(wwwroot, uri);
    if (!set_resource_info())
    {
        return false;
    }
    if (!set_resource_content())
    {
        return false;
    }
    if (!set_mime_code())
    {
        return false;
    }

    // Adding resource dependent headers
    RESPONSE_AddHeader("Content-Type", PROCESS_GetMime());

    char snum[32];
    memset(snum, 0, sizeof(snum));
    snprintf(snum, sizeof(snum), "%ld", PROCESS_GetFileSize());
    RESPONSE_AddHeader("Content-Length", snum);

    return true;
}

/*
    Return a pointer to the resource content.
    don't free this.
*/
char *PROCESS_GetResourceContent(void)
{
    return process->resource_content;
}

size_t PROCESS_GetFileSize(void)
{
    return process->resource_info.st_size;
}

char *PROCESS_GetMime(void)
{
    return process->mime_str;
}

/* 
lwd_status_code_e RESOURCE_ExecCGI(lwd_resource_t *rsrc)
{
    FILE *pfd;
    char buf[BUFSIZ/2];
    int n;
    memset(buf, 0, sizeof(buf));

    pfd = popen(rsrc->path_to_file, "r");
    if (!pfd)
    {
        perror("Error");
        return INTERNAL_SERVER_ERROR;
    }

    n = fread(buf, 1, sizeof(buf), pfd);

    pclose(pfd);

    printf("=========OUTPUT bytes read : %d=======\n%s\n", n, buf);

    return OK;
} */