#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include "../../../../useful/useful.h"

#include "response.h"
#include "../mime/mime.h"
#include "../header.h"
#include "../process/process.h"

lwd_list_t *headers = NULL;

/* 
    Return NULL on error
*/
static char *format_status_line(lwd_response_t *r)
{
    size_t len = strlen(r->http_version) + strlen(r->status_str) + 10;
    char tmpbuf[len];
    memset(tmpbuf, 0, sizeof(tmpbuf));

    if (snprintf(tmpbuf, sizeof(tmpbuf), "%s %d %s\r\n", r->http_version, r->status_code, r->status_str) < 0)
    {
        printf("%s:%d : Error : snprintf() failed.\n", __FILE__, __LINE__);
        return NULL;
    }

    char *buf;
    len = strlen(tmpbuf);
    buf = (char *)calloc(len+1, sizeof(char));
    strncpy(buf, tmpbuf, len);
    buf[len] = 0;

    return buf;
}

/* 
    Return NULL on error
*/
static char *FormatResponse(lwd_response_t *response)
{
    Item *cur;
    lwd_response_header_t *rh;
    char buf[BUFSIZ];
    char tmpbuf[BUFSIZ];
    char *status_line;
    char *res;

    memset(buf, 0, sizeof(buf));
    memset(tmpbuf, 0, sizeof(buf));
    
    status_line = format_status_line(response);
    if (!status_line)
    {
        return NULL;
    }
    strcat(buf, status_line);
    free(status_line);

    cur = headers->begin;
    while (cur)
    {
        rh = (lwd_response_header_t *)cur->value;
        snprintf(tmpbuf, sizeof(tmpbuf), "%s: %s\r\n", rh->field, rh->value);
        strcat(buf, tmpbuf);
        memset(tmpbuf, 0, sizeof(tmpbuf));
        cur = cur->next;
    }
    strcat(buf, "\r\n");

    res = (char *)calloc(strlen(buf)+1, sizeof(char));
    strcpy(res, buf);

    return res;
}

lwd_response_t *RESPONSE_Init(void)
{
    lwd_response_t *response = (lwd_response_t *)calloc(1, sizeof(lwd_response_t));
    
    if (headers == NULL)
    {
        headers = DLL_NewList();
    }

    return response;
}

static void freeKey(Item *item)
{
    if (!item->value)
        return;
    lwd_response_header_t *header = (lwd_response_header_t *)item->value;

    if (header->field)
    {
        free(header->field);
        header->field = NULL;
    }
    if (header->value)
    {
        free(header->value);
        header->value = NULL;
    }
    free(header);
}

static void RESPONSE_FreeHeaderList(void)
{
    DLL_Free(headers, freeKey);
}

void RESPONSE_Free(lwd_response_t *response)
{
    if (headers)
    {
        RESPONSE_FreeHeaderList();
        headers = NULL;
    }

    if (!response)
        return;

    if (response->status_str)
    {
        free(response->status_str);
        response->status_str = NULL;
    }
    if (response->http_version)
    {
        free(response->http_version);
        response->http_version = NULL;
    }
    if (response->response)
        free(response->response);
    free(response);
}

static void set_status_code(lwd_response_t *r)
{
    r->status_code = http_status;
    r->status_str = str_status_code(http_status);
}

static void set_http_version(lwd_response_t *r, char *http_version)
{
    r->http_version = alloc_n_cpy(http_version, strlen(http_version));
}

void RESPONSE_Set(lwd_response_t *response, const char *http_version)
{
    set_status_code(response);
    set_http_version(response, (char *)http_version);
}

bool RESPONSE_GenerateResponse(lwd_response_t *response)
{
    response->response = FormatResponse(response);
    if (response->response == NULL)
    {
        printf("%s:%d : FormatResponse() failed.\n", __FILE__, __LINE__);
        http_status = INTERNAL_SERVER_ERROR;
        return false;
    }

    return true;
}

bool RESPONSE_AddHeader(char *field, char *value)
{
    if (!field)
    {
        printf("field is empty.\n");
        return false;
    }

    lwd_response_header_t *header = (lwd_response_header_t *)calloc(1, sizeof(lwd_response_header_t));
    size_t len;

    // Copy field
    len = strlen(field);
    header->field = (char *)calloc(len+1, sizeof(char));
    strncpy(header->field, field, len);

    // Copy value
    len = strlen(value);
    if (!len)
    {
        DLL_AddRight(headers, (void *)header);
        return true;
    }
    header->value = (char *)calloc(len+1, sizeof(char));
    strncpy(header->value, value, len);

    DLL_AddRight(headers, (void *)header);

    return true;
}

static void PrintHeader(Item *item)
{
    lwd_response_header_t *header = (lwd_response_header_t *)item->value;
    printf("Key: %s\nValue: %s\n", header->field, header->value);
}

void RESPONSE_PrintHeaderList(void)
{
    DLL_ForEach(headers, PrintHeader);
}