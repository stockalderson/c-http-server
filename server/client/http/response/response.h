#ifndef __LWD_RESPONSE_H__
#define __LWD_RESPONSE_H__

#include <sys/stat.h>
#include "../../../../error/error.h"
#include "../../../../dll/dll.h"

typedef struct {
    char *field;
    char *value;
} lwd_response_header_t;

typedef struct {
    lwd_status_code_e status_code;
    char *status_str;
    char *http_version;

    char *response;
    int fdindex;
} lwd_response_t;

lwd_response_t *RESPONSE_Init(void);
void RESPONSE_Free(lwd_response_t *response);

void RESPONSE_Set(lwd_response_t *response, const char *http_version);
bool RESPONSE_GenerateResponse(lwd_response_t *response);
bool RESPONSE_AddHeader(char *field, char *value);
void RESPONSE_PrintHeaderList(void);

#endif