#include <stdio.h>
#include <stdlib.h>

#include "header.h"

static void freeItem(Item *header);

void HEADER_FreeHeaderList(lwd_list_t *list)
{
    if (list)
    {
        DLL_Free(list, freeItem);
    }
}

static void freeItem(Item *item)
{
    if (VALUE(item))
    {
        free(VALUE(item)->key);
        free(VALUE(item)->value);
    }
    free(item->value);
}