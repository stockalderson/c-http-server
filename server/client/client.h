#ifndef __LWD_CLIENT_H__
#define __LWD_CLIENT_H__

#include <netinet/in.h>
#include <stdbool.h>

#define TIMEOUT 500000UL // in microseconds

typedef struct {
    int rsockfd;
    struct sockaddr_in addr;
    int addr_len;
    char data[2*(1<<12)];
} lwd_client_t;

bool CLIENT_NewConnection(int server_sockfd, char *wwwroot);

#endif