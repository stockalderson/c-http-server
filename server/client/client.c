#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <sys/time.h>
#include "../../error/error.h"

#include "client.h"
#include "../../useful/useful.h"
#include "http/http.h"
#include "../logs/logs.h"

static lwd_client_t *CLIENT_New(void);
static void CLIENT_Free(lwd_client_t *client);
static bool CLIENT_AcceptConnection(lwd_client_t *client, int server_sockfd);
static void CLIENT_CloseConnection(lwd_client_t *client);
static bool CLIENT_SetSocketOption(lwd_client_t *client, int level, int optname, const void *opt_val, socklen_t opt_len);
static bool CLIENT_Handle(lwd_client_t *client, char *wwwroot);
static bool HandleRequest(lwd_client_t *client, char *wwwroot);

static lwd_client_t *CLIENT_New(void)
{
    lwd_client_t *client = malloc(sizeof(lwd_client_t));
    memset(client, 0, sizeof(*client));
    memset(&client->addr, 0, sizeof(client->addr));
    client->addr_len = sizeof(client->addr);
    client->rsockfd = -1;
    memset(&client->data, 0, sizeof(client->data));

    return client;
}

static void CLIENT_Free(lwd_client_t *client)
{
    if (!client)
    {
        return;
    }
    free(client);
}

static bool CLIENT_AcceptConnection(lwd_client_t *client, int server_sockfd)
{
    if (!client)
    {
        printf("%s:%d : URGENT Error : client is null.\n", __FILE__, __LINE__);
        return false;
    }
    if (server_sockfd < 0)
    {
        printf("%s:%d : URGENT Error : server socket isn't valid.\n", __FILE__, __LINE__);
        return false;
    }

    int rsockfd = -1;

    rsockfd = accept(server_sockfd, (struct sockaddr *)&client->addr, (socklen_t *)&client->addr_len);
    if (rsockfd == -1)
    {
        if (lwd_error == LWD_ERROR_INTERRUPT)
            return false;
        if (errno == EBADF)
            lwd_error = LWD_ERROR_BAD_FD;
        printf("%s:%d : accept() failed : %d %s\n", __FILE__, __LINE__, errno, strerror(errno));
        return false;
    }
    client->rsockfd = rsockfd;

    return true;
}

static void CLIENT_CloseConnection(lwd_client_t *client)
{
    if (client->rsockfd > -1)
    {
        if (shutdown(client->rsockfd, SHUT_RDWR) == -1)
        {
            printf("ERROR : Couldn't shutdown client socket.\n");
        }
        close(client->rsockfd);
        client->rsockfd = -1;
    }
}

static bool CLIENT_SetSocketOption(lwd_client_t *client, int level, int optname, const void *opt_val, socklen_t opt_len)
{
    if (!client)
    {
        printf("%s:%d : URGENT Error : client is null.\n", __FILE__, __LINE__);
        return false;
    }

    if (setsockopt(client->rsockfd, level, optname, opt_val, opt_len) == -1)
    {
        printf("%s:%d : setsockopt() failed : %s\n", __FILE__, __LINE__, strerror(errno));
        return false;
    }

    return true;
}

static bool CLIENT_Handle(lwd_client_t *client, char *wwwroot)
{
    if (!wwwroot || !(*wwwroot))
    {
        printf("%s:%d : Error : wwwroot is null or empty.\n", __FILE__, __LINE__);
        http_status = INTERNAL_SERVER_ERROR;
        return false;
    }

    if (recv(client->rsockfd, client->data, sizeof(client->data), 0) == -1)
    {
        printf("%s:%d : recv() failed : %s\n", __FILE__, __LINE__, strerror(errno));
        return false;
    }
    if (*client->data == 0)
    {
        printf("No Request.\n");
        return false;
    }

    printf("Request: \n%s\n\n", client->data);

    if (!HandleRequest(client, wwwroot))
    {
        printf("%s:%d : Error : HandleRequest() failed.\n", __FILE__, __LINE__);
        return false;
    }

    /* Leave */

    return true;
}

static bool HandleRequest(lwd_client_t *client, char *wwwroot)
{
    if (!client)
    {
        printf("client is null.\n");
        goto FatalError;
    }

    /* Declaration */

    lwd_http_t *http;

    /* Initialisation */

    http = HTTP_New();

    /* Parsing */

    // Parse Method, Headers and Body.

    if (HTTP_IsTLS(client->data))
    {
        printf("Peer is possibly trying to communicate over TLS.\n");
        if (!HTTP_HandleTCPHandshake(client->data, client->rsockfd))
        {
            http_status = FORBIDDEN;
            goto FatalError;
        }
        http_status = FORBIDDEN;
        goto FatalError;
    }

    if (!HTTP_ParseRequest(http, client->data))
    {
        printf("%s:%d : HTTP_ParseRequest() failed.\n", __FILE__, __LINE__);
        goto FatalError;
    }

    /* Processing */

    if (!HTTP_ProcessRequest(http, wwwroot))
    {
        printf("%s:%d : Error : HTTP_ProcessRequest() failed.\n", __FILE__, __LINE__);
        goto FatalError;
    }

    if (!HTTP_GenerateResponse(http))
    {
        printf("%s:%d : Error : HTTP_GenerateResponse() failed.\n", __FILE__, __LINE__);
        goto FatalError;
    }

    printf("\nResponse :\n%s", http->response->response);

    // Send Response
    if (!HTTP_SendResponse(http, client->rsockfd))
    {
        printf("%s:%d : HTTP_SendResponse() failed.\n", __FILE__, __LINE__);
        goto FatalError;
    }

    /* Leave */

    if (http)
    {
        http = HTTP_Free(http);
    }

    return true;

FatalError:

    printf("\nFATAL ERROR.\n\n");

    HTTP_ErrorPage(http, client->rsockfd);

    if (http != NULL)
    {
        http = HTTP_Free(http);
    }

    return false;
}

bool CLIENT_NewConnection(int server_sockfd, char *wwwroot)
{
    struct timeval recv_timeout;

    memset(&recv_timeout, 0, sizeof(recv_timeout));
    recv_timeout.tv_usec = TIMEOUT;
    recv_timeout.tv_sec = 0.5;
    http_status = OK;

    lwd_client_t *client = CLIENT_New();

    if (!CLIENT_AcceptConnection(client, server_sockfd))
    {
        if (lwd_error == LWD_ERROR_INTERRUPT)
            goto Interrupt;
        if (lwd_error == LWD_ERROR_BAD_FD)
            goto FatalError;
        printf("%s:%d : Error : CLIENT_AcceptConnection() failed.\n", __FILE__, __LINE__);
        CLIENT_Free(client);
        client = NULL;
        return true;
    }
    printf("Connection accepted.\n");

    // Set receive timeout
    if (!CLIENT_SetSocketOption(client, SOL_SOCKET, SO_RCVTIMEO, &recv_timeout, sizeof(recv_timeout)))
    {
        printf("%s:%d : Error : CLIENT_SetSocketOption() failed.\n", __FILE__, __LINE__);
    }

    if (!CLIENT_Handle(client, wwwroot))
    {
        printf("%s:%d : Error : CLIENT_Handle() failed.\n", __FILE__, __LINE__);
    }
    LOGS_Update(http_status, client->addr);

    CLIENT_CloseConnection(client);
    CLIENT_Free(client);
    client = NULL;
    printf("----------------------CLIENT-FREED--------------------------\n");
    return true;

Interrupt:
    if (client)
    {
        CLIENT_Free(client);
        client = NULL;
    }
    return false;

FatalError:
    if (client)
    {
        CLIENT_Free(client);
        client = NULL;
    }
    return false;
}