#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "dll.h"

lwd_list_t *DLL_NewList(void)
{
    return (lwd_list_t *)calloc(1, sizeof(lwd_list_t));
}

// FreeItems free all items struct
void DLL_Clear(lwd_list_t *list, void(*freeval)(Item *item))
{
    if (!list || list->begin == NULL)
        return;

    Item *cur = list->begin;
    Item *next;
   
    while (cur)
    {
        next = cur->next;
        if (freeval)
            freeval(cur);
        free(cur);
        list->count--;
        cur = next;
    }
    list->begin = NULL;

    return;
}

void DLL_Free(lwd_list_t *list, void(*freeval)(Item *item))
{
    if (!list)
        return;

    DLL_Clear(list, freeval);
    free(list);

    return;
}

void DLL_ForEach(lwd_list_t *list, void(*f)(Item *item))
{
    Item *item;

    item = list->begin;
    for (int i=0 ; i<list->count ; i++)
    {
        f(item);
        item = item->next;
    }
}

void DLL_AddRight(lwd_list_t *list, void *value)
{
    if (!list)
        return;

    if (list->count == 0)
    {
        Item *item = (Item *)calloc(1, sizeof(Item));
        item->value = value;
        list->begin = item;
        list->end = item;
        list->count++;
        return;
    }

    Item *cur = (Item *)calloc(1, sizeof(Item));
    cur->value = value;
    cur->prev = list->end;
    cur->prev->next = cur;
    list->end = cur;
    list->count++;

    return;
}

lwd_list_t *DLL_AddLeft(lwd_list_t *item);

lwd_list_t *DLL_PopRight(lwd_list_t *item);

lwd_list_t *DLL_PopLeft(lwd_list_t *item);

size_t DLL_Length(lwd_list_t *list)
{
    if (!list)
        return 0;

    return list->count;
}

Item *DLL_Find(lwd_list_t *list, void *value)
{
    if (!list)
        return NULL;

    Item *cur = list->begin;

    while (cur)
    {
        if (cur->value == value)
        {
            return cur;
        }

        cur = cur->next;
    }

    return NULL;
}