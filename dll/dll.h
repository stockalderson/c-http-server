#ifndef __LWD_DOUBLE_LINKED_LIST_H__
#define __LWD_DOUBLE_LINKED_LIST_H__

#include <stdbool.h>

struct Item {
    struct Item *prev;
    struct Item *next;
    void *value;
};

typedef struct Item Item;

typedef struct {
    Item *begin;
    Item *end;
    int count;
} lwd_list_t;

lwd_list_t *DLL_NewList(void);
void DLL_Clear(lwd_list_t *list, void(*freeval)(Item *item));
void DLL_Free(lwd_list_t *list, void(*freeval)(Item *item));
void DLL_ForEach(lwd_list_t *list, void(*f)(Item *item));
void DLL_AddRight(lwd_list_t *list, void *value);
Item *DLL_Find(lwd_list_t *list, void *value);
lwd_list_t *DLL_AddLeft(lwd_list_t *list);
lwd_list_t *DLL_PopRight(lwd_list_t *list);
lwd_list_t *DLL_PopLeft(lwd_list_t *list);
size_t DLL_Length(lwd_list_t *list);

#endif