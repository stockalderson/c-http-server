#ifndef __LWD_FSM_H__
#define __LWD_FSM_H__

#define LOOP_COUNT_MAX (100)

typedef struct {
    int state;
    int loop_count;
} lwd_fsm_t;

lwd_fsm_t *FSM_New(void);
lwd_fsm_t *FSM_Free(lwd_fsm_t *fsm);

#endif