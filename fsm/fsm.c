#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "fsm.h"


lwd_fsm_t *FSM_New(void)
{
    lwd_fsm_t *fsm = malloc(sizeof(lwd_fsm_t));
    memset(fsm, 0, sizeof(*fsm));
    fsm->state = 0;
    fsm->loop_count = 0;

    return fsm;
}

lwd_fsm_t *FSM_Free(lwd_fsm_t *fsm)
{
    if (fsm == NULL)
    {
        return NULL;
    }

    free(fsm);

    return NULL;
}